﻿using System.ComponentModel;
using Petrogaz.UWP.Renderers;
using Petrogaz.Views;
using Windows.UI;
using Windows.UI.Xaml.Media;
using Xamarin.Forms;
using Xamarin.Forms.Platform.UWP;


[assembly: ExportRenderer(typeof(LinePicker), typeof(LinePickerRendererUWP))]
namespace Petrogaz.UWP.Renderers
{
    public class LinePickerRendererUWP : PickerRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
        {
            base.OnElementChanged(e);
            if(Control != null)
            {
                Control.BorderThickness = new Windows.UI.Xaml.Thickness(0);
                Control.Background = new SolidColorBrush(Colors.Transparent);
                OnElementPropertyChanged(Element, new PropertyChangedEventArgs("HorizontalTextAlignment"));
            }
        }
        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            if (Control == null) { return; }
            if (e.PropertyName == "HorizontalTextAlignment")
            {
                Control.HorizontalContentAlignment = ConvertXamarinToUWPAlignment((Element as LinePicker).HorizontalTextAlignment);
            }
        }
        private Windows.UI.Xaml.HorizontalAlignment ConvertXamarinToUWPAlignment(TextAlignment alignment)
        {
            if(alignment == TextAlignment.Center)
            {
                Control.Padding = new Windows.UI.Xaml.Thickness(32, 0, 0, 0);
                return Windows.UI.Xaml.HorizontalAlignment.Center;
            }
            Control.Padding = new Windows.UI.Xaml.Thickness(0, 0, 0, 0);
            if (alignment == TextAlignment.Start)
            {
                return Windows.UI.Xaml.HorizontalAlignment.Left;
            }
            return Windows.UI.Xaml.HorizontalAlignment.Right;
        }
    }
}
