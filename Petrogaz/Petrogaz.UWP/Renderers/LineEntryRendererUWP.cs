﻿
using Petrogaz.UWP.Renderers;
using Petrogaz.Views;
using Windows.UI;
using Windows.UI.Xaml.Media;
using Xamarin.Forms;
using Xamarin.Forms.Platform.UWP;

[assembly:ExportRenderer(typeof(LineEntry), typeof(LineEntryRendererUWP))]
namespace Petrogaz.UWP.Renderers
{
    public class LineEntryRendererUWP : EntryRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            if(Control != null)
            {
                Control.BorderThickness = new Windows.UI.Xaml.Thickness(0);
                Control.Background = new SolidColorBrush(Colors.Transparent);
                Control.BackgroundFocusBrush = new SolidColorBrush(Colors.Transparent);
            }
        }
    }
}
