﻿using Petrogaz.UWP.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.UWP;


[assembly: ExportRenderer(typeof(Switch), typeof(SwitchRendererUWP))]
namespace Petrogaz.UWP.Renderers
{
    public class SwitchRendererUWP : SwitchRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Switch> e)
        {
            base.OnElementChanged(e);
            if(Control != null)
            {
                Control.OffContent = " ";
                Control.OnContent = " ";
                Control.FlowDirection = Windows.UI.Xaml.FlowDirection.RightToLeft;
            }
        }
    }
}
