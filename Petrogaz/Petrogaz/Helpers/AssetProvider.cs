﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Petrogaz.Helpers
{
    [ContentProperty(nameof(Name))]
    public class AssetProvider : IMarkupExtension
    {
        public static string GetAsset(string assetName)
        {
            if (assetName == null) { return null; }
            if (Device.RuntimePlatform == Device.UWP)
            {
                return $"Assets/{assetName}";
            }
            return assetName;
        }
        public string Name { get; set; }
        public object ProvideValue(IServiceProvider serviceProvider) => GetAsset(Name);
    }
}
