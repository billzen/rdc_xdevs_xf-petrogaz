﻿namespace Petrogaz.Helpers
{
    public static class Output
    {
        public static void Print(object obj)
        {
#if DEBUG
            System.Diagnostics.Debug.WriteLine($"----> {obj}");
#endif
        }
    }
}
