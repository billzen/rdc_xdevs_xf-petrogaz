﻿using Petrogaz.Views;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;

namespace Petrogaz.Helpers
{
    public class ViewHelper
    {
        public static ViewHelper Current { get; } = new ViewHelper();

        #region Colors
        public static Color ColorBlue { get; } = Color.FromHex("#0E3E83");
        public static Color ColorRed { get; } = Color.FromHex("#983628");
        public static Color ColorGray { get; } = Color.FromHex("#676767");
        public static Color ColorLightGray { get; } = Color.FromHex("#BEBEBE");
        #endregion

        #region Styles
        public static Style LabelStyle { get; } = new Style(typeof(Label))
        {
            Setters =
            {
                new Setter { Property = Label.TextColorProperty, Value = ColorGray },
                new Setter { Property = Label.FontSizeProperty, Value = FontHelper.NormalSize },
                new Setter { Property = Label.FontFamilyProperty, Value = FontHelper.MainRegularFamily }
            }
        };
        public static Style EntryStyle { get; } = new Style(typeof(Xamarin.Forms.Entry))
        {
            Setters =
            {
                new Setter { Property = Xamarin.Forms.Entry.TextColorProperty, Value = ColorGray },
                new Setter { Property = Xamarin.Forms.Entry.FontSizeProperty, Value = FontHelper.NormalSize }
            }
        };
        public static Style LineEntryStyle { get; } = new Style(typeof(LineEntry))
        {
            BasedOn = EntryStyle
        };
        public static Style LinePickerStyle { get; } = new Style(typeof(LinePicker))
        {
            Setters =
            {
                new Setter { Property = Xamarin.Forms.Entry.TextColorProperty, Value = ColorGray },
                new Setter { Property = Xamarin.Forms.Entry.FontSizeProperty, Value = FontHelper.NormalSize }
            }
        };
        public static Style ButtonStyle { get; } = new Style(typeof(Button))
        {
            Setters =
            {
                new Setter { Property = Button.BackgroundColorProperty, Value = ColorBlue },
                new Setter { Property = Button.CornerRadiusProperty, Value = 0 },
                new Setter { Property = Button.TextColorProperty, Value = Color.White },
                new Setter { Property = Button.HeightRequestProperty, Value = 52 }
            }
        };
        #endregion

        public void Initialize()
        {
            App.Current.Resources.Add(LabelStyle);
            App.Current.Resources.Add(EntryStyle);
            App.Current.Resources.Add(LineEntryStyle);
            App.Current.Resources.Add(LinePickerStyle);
        }

        private Thickness _safePadding;
        public Thickness GetSafePadding(Xamarin.Forms.Page page)
        {
            if (Device.RuntimePlatform != Device.iOS) { return _safePadding; }
            if (_safePadding.Top == 0)
            {
                if (DeviceInfo.VersionString.StartsWith("10"))
                {
                    _safePadding = new Thickness(0, 20, 0, 0);
                }
                else
                {
                    Thickness padding = page.On<iOS>().SafeAreaInsets();
                    _safePadding = new Thickness(0, padding.Top, 0, 0);
                }
            }
            return _safePadding;
        }
    }
}
