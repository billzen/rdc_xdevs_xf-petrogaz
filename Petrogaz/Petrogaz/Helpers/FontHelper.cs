﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Petrogaz.Helpers
{
    public static class FontHelper
    {
        public static float NanoFactor { get; set; } = .4f;
        public static float TinyFactor { get; set; } = .6f;
        public static float SmallFactor { get; set; } = .8f;
        public static float NormalFactor { get; set; } = 1f;
        public static float LargeFactor { get; set; } = 1.2f;
        public static float HugeFactor { get; set; } = 1.4f;
        public static float TeraFactor { get; set; } = 1.6f;

        public static double NanoSize => Math.Round(BaseSize * NanoFactor);
        public static double TinySize => Math.Round(BaseSize * TinyFactor);
        public static double SmallSize => Math.Round(BaseSize * SmallFactor);
        public static double NormalSize => Math.Round(BaseSize * NormalFactor);
        public static double LargeSize => Math.Round(BaseSize * LargeFactor);
        public static double HugeSize => Math.Round(BaseSize * HugeFactor);
        public static double TeraSize => Math.Round(BaseSize * TeraFactor);

        public static double BaseSize = Device.GetNamedSize(NamedSize.Default, typeof(Label));

        #region Families
        private static string _mainBoldFamily;
        public static string MainBoldFamily
        {
            private set => _mainBoldFamily = value;
            get
            {
                if (string.IsNullOrEmpty(_mainBoldFamily))
                {
                    if (Device.RuntimePlatform == Device.iOS)
                    {
                        _mainBoldFamily = "SF-Pro-Display-Regular";
                    }
                    else if (Device.RuntimePlatform == Device.Android)
                    {
                        _mainBoldFamily = "SF-Pro-Display-Regular.otf#SF Pro Display";
                    }
                }
                return _mainBoldFamily;
            }
        }
        private static string _mainRegularFamily;
        public static string MainRegularFamily
        {
            private set => _mainRegularFamily = value;
            get
            {
                if (string.IsNullOrEmpty(_mainRegularFamily))
                {
                    if (Device.RuntimePlatform == Device.iOS)
                    {
                        _mainRegularFamily = "SF-Pro-Display-Light";
                    }
                    else if (Device.RuntimePlatform == Device.Android)
                    {
                        _mainRegularFamily = "SF-Pro-Display-Light.otf#SF Pro Display";
                    }
                }
                return _mainRegularFamily;
            }
        }
        private static string _mainHeavyFamily;
        public static string MainHeavyFamily
        {
            private set => _mainHeavyFamily = value;
            get
            {
                if (string.IsNullOrEmpty(_mainHeavyFamily))
                {
                    if (Device.RuntimePlatform == Device.iOS)
                    {
                        _mainHeavyFamily = "SF-Pro-Display-Heavy";
                    }
                    else if (Device.RuntimePlatform == Device.Android)
                    {
                        _mainHeavyFamily = "SF-Pro-Display-Heavy.otf#SF Pro Display";
                    }
                }
                return _mainHeavyFamily;
            }
        }
        #endregion
    }
}
