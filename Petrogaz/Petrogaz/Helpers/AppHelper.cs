﻿using Petrogaz.Abstractions;
using Prism.Ioc;
using Xrx.Localization.Abstractions;

namespace Petrogaz.Helpers
{
    public static class AppHelper
    {
        public static IContainerProvider Container => App.Current.Container;
        public static ILocalizationService Localizer => Container.Resolve<ILocalizationService>();
    }
}
