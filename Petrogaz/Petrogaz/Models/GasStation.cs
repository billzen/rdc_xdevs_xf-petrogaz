﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Petrogaz.Models
{
    public class GasStation
    {
        public string id { get; set; }
        public string erp_code { get; set; }
        public string erp_sub_id { get; set; }
        public string company_name { get; set; }
        public string city { get; set; }
        public string address { get; set; }
        public string zip_code { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
        public string telephone1 { get; set; }
        public string telephone2 { get; set; }
        public string mobilephone { get; set; }
        public string email { get; set; }
        public string tin { get; set; }
        public string fax { get; set; }
        public string product_ids { get; set; }
        public List<LocalizedName> localized_names { get; set; }
    }


    public class RootObjectGasStation
    {
        public List<GasStation> gasstations { get; set; }
    }
}
