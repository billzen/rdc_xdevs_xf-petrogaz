﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Petrogaz.Models
{
    public class GasProduct
    {
        public string id { get; set; }
        public string erp_code { get; set; }
        public string name { get; set; }
        public List<LocalizedName> localized_names { get; set; }
    }

    public class RootObjectGasProducts
    {
        public List<GasProduct> products { get; set; }
    }
}
