﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Petrogaz.Models
{
    public class LocalizedName
    {
        public int language_id { get; set; }
        public string localized_companyname { get; set; }
        public string localized_address { get; set; }
        public string localized_city { get; set; }
    }
}
