﻿using Petrogaz.Helpers;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Petrogaz.Views
{
    public class BasePage : ContentPage
    {
        public BasePage()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            NavigationPage.SetHasBackButton(this, false);
            BackgroundColor = Color.FromHex("#F5F5F5");
        }
        protected override void LayoutChildren(double x, double y, double width, double height)
        {
            base.LayoutChildren(x, y, width, height);
            Padding = ViewHelper.Current.GetSafePadding(this);
        }
    }
}
