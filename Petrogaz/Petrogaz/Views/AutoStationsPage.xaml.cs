﻿//using Xamarin.Essentials;
using Petrogaz.ViewModels;
using System;
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;
using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks;
using Petrogaz.Abstractions;
using RestSharp;
using Newtonsoft.Json;
using System.Collections.Generic;
using Prism.Navigation;

namespace Petrogaz.Views
{
    public partial class AutoStationsPage : BasePage
    {

        public bool mapinitok = false;
        public int state = 0;
        public Map map;

        private readonly INavigationService _navigationService;
        private readonly AutoStationsPageViewModel vm;

        public AutoStationsPage(INavigationService navigationService)
        {

            InitializeComponent();

            mapinitok = false;
            this._navigationService = navigationService;
            vm = new AutoStationsPageViewModel(_navigationService);


            map = new Map()
            {
                MyLocationEnabled = false
            };

            ActivityIndicator indicator = this.FindByName<ActivityIndicator>("ActivityIndicatorMap");
            indicator.IsEnabled = true;
            indicator.IsRunning = true;
            indicator.IsVisible = true;

            popup.SetState(PopupState.Closed);
            state = 0;
            popup.IsVisible = false;

            populatePinsCollection();

            MessagingCenter.Subscribe<string>(this, "pinsLoadedGasStations", (sender) =>
            {
                int numofpins = InitPinsInMap();

                var zoomLevel = 5; // pick a value between 1 and 18
                var latlongdeg = 360 / (Math.Pow(2, zoomLevel));
                map.MoveToRegion(new MapSpan(new Position(38, 23), latlongdeg, latlongdeg));

                map.MapClicked += Map_MapClicked;
                map.PinClicked += Pinitem_Clicked;

                mapinitok = true;


                Device.BeginInvokeOnMainThread(() =>
                {
                    StackLayoutMap.Children.Add(map);
                    indicator.IsEnabled = false;
                    indicator.IsRunning = false;
                    indicator.IsVisible = false;
                });

            });


            popup.SetState(PopupState.Closed);
            state = 0;
            popup.IsVisible = false;
        }

        public void populatePinsCollection()
        {
            System.Threading.Tasks.Task.Run(async () =>
            {
                await vm.populatePins();
            });
        }

        private int InitPinsInMap()
        {
            int aCount = 0;
            if (vm.Pins.Count > 0)
            {

                foreach (Pin pinitem in vm.Pins)
                {
                    map.Pins.Add(pinitem);
                    aCount++;
                }
            }
            return aCount;
        }

        private async void Pinitem_Clicked(object sender, Xamarin.Forms.GoogleMaps.PinClickedEventArgs e)
        {
            e.Handled = true;
            popup.SetState(PopupState.FullOpen);
            state = 1;
            popup.IsVisible = true;
            grid.ForceLayout();
            await Task.Delay(300);
            Xamarin.Forms.GoogleMaps.PinClickedEventArgs ea = e as Xamarin.Forms.GoogleMaps.PinClickedEventArgs;

            if (ea != null)
            {

                var zoomLevel = 8; // pick a value between 1 and 18
                var latlongdeg = 360 / (Math.Pow(2, zoomLevel));
                await Task.Delay(300);
                map.MoveToRegion(new MapSpan(new Position(ea.Pin.Position.Latitude, ea.Pin.Position.Longitude), latlongdeg, latlongdeg));

                Xamarin.Forms.Label nl = popup.FindByName("NameLabel") as Xamarin.Forms.Label;
                Xamarin.Forms.Label il = popup.FindByName("InfoLabel") as Xamarin.Forms.Label;
                Xamarin.Forms.Label cl = popup.FindByName("ContactLabel") as Xamarin.Forms.Label;
                if (nl != null) nl.Text = ea.Pin.Label;
                if (il != null) il.Text = ea.Pin.Address;
                if (ea.Pin.Tag != null && cl != null) cl.Text = ea.Pin.Tag.ToString();
            }
        }


        private void Map_MapClicked(object sender, MapClickedEventArgs e)
        {
            popup.SetState(PopupState.Closed);
            state = 0;
            popup.IsVisible = false;
            grid.ForceLayout();
            var zoomLevel = 5; // pick a value between 1 and 18
            var latlongdeg = 360 / (Math.Pow(2, zoomLevel));
            map.MoveToRegion(new MapSpan(new Position(38, 23), latlongdeg, latlongdeg));
        }

    }



}


