﻿using Petrogaz.Helpers;
using Xamarin.Forms;

namespace Petrogaz.Views
{
    public class BaseListView : ListView
    {
        public BaseListView()
        {
            SeparatorColor = ViewHelper.ColorLightGray;
            HasUnevenRows = true;
            BackgroundColor = Color.White;
            ItemTapped += ListItemTapped;
        }

        private void ListItemTapped(object sender, ItemTappedEventArgs e)
        {
            SelectedItem = null;
        }
    }
}
