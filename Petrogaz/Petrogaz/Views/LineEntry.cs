﻿using Petrogaz.Abstractions;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Xamarin.Forms;

namespace Petrogaz.Views
{
    public class LineEntry : Entry, IAnimatedControlContent
    {
        public ICommand TriggeredCommand
        {
            get { return (ICommand)GetValue(TriggeredCommandProperty); }
            set { SetValue(TriggeredCommandProperty, value); }
        }

        public bool HasText => Text?.Length > 0;

        public static readonly BindableProperty TriggeredCommandProperty =
            BindableProperty.Create(nameof(TriggeredCommand), 
                                    typeof(ICommand), 
                                    typeof(LineEntry));

        public LineEntry()
        {
            if (Device.RuntimePlatform == Device.iOS)
            {
                HeightRequest = 30;
            }
        }

        protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);
            if (propertyName == nameof(IsFocused))
            {
                ControlTriggered(IsFocused);
            }
            else if (propertyName == nameof(Text))
            {
                ControlTriggered(Text?.Length > 0);
            }
            else if (propertyName == nameof(Placeholder))
            {
                Placeholder = null;
            }
        }
        private void ControlTriggered(bool isFocused)
        {
            if (TriggeredCommand != null &&
                TriggeredCommand.CanExecute(null))
            {
                TriggeredCommand.Execute(isFocused);
            }
        }
        
    }
}
