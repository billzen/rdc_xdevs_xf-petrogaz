﻿using Petrogaz.Helpers;
using Petrogaz.Localization;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Petrogaz.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LearnLogo : ContentView
	{
        public LearnLogo ()
		{
			InitializeComponent ();
            labelSpanPartOne.Text = AppHelper.Localizer.GetString(Strings.LearnAboutKey) + Environment.NewLine;
            labelSpanPartTwo.Text = AppHelper.Localizer.GetString(Strings.LiquidGasKey);
            image.SizeChanged += ImageSizeChanged;
		}

        private void ImageSizeChanged(object sender, EventArgs e)
        {
            if (image != null)
            {
                label.Margin = new Thickness(0, image.Height * .1, 0, 0);
                labelSpanPartOne.FontSize = image.Height * .1 - 3;
                labelSpanPartTwo.FontSize = image.Height * .1 - 3;
            }
            
        }
    }
}