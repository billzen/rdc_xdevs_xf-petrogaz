﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Petrogaz.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MapPopup : Grid
    {
        public MapPopup()
        {
            InitializeComponent();
            IsVisible = false;

        }

   
        public PopupState State { get; private set; }
        public async void SetState(PopupState value)
        {
            if (State == value) { return; }
            State = value;
            if (State.PopupVisible) { IsVisible = State.PopupVisible; }
            await Task.Delay(0);
          //  await this.TranslateTo(0, State.PopupY());
            if (!State.PopupVisible) { IsVisible = State.PopupVisible; }
        }
    }
    public class PopupState
    {
        public static PopupState Closed = new PopupState
        {
            ButtonRotation = 0,
            //PopupY = () => ParentPageHeight,
            PopupVisible = false
        };
        public static PopupState FullOpen = new PopupState
        {
            ButtonRotation = 180,
            // PopupY = () => ParentPageHeight - 455,
            PopupVisible = true
        };
        public static double ParentPageHeight { get; set; } = 0;
        public int ButtonRotation { get; set; }
        //  public Func<double> PopupY { get; set; }
        public bool PopupVisible { get; set; }
    }


}