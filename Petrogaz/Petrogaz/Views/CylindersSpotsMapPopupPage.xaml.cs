﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Petrogaz.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CylindersSpotsMapPopupPage : Grid
    {
        public CylindersSpotsMapPopupPage()
        {
            InitializeComponent();
            IsVisible = false;

            // invokeButton.ImageSource = 
       //     invokeButton.SetValue(FrameButton.CommandProperty, new Command(OnInvokeButtonClicked));

           

        }

        private void OnInvokeButtonClicked()
        {
            if (State == PopupStateFull.SemiOpen)
            {
                SetState(PopupStateFull.FullOpen);

            }
            else if (State == PopupStateFull.FullOpen)
            {
                SetState(PopupStateFull.SemiOpen);
            }
        }


        public void OnInvokeButtonClickedFromParent()
        {
            if (this.State == PopupStateFull.FullOpen)
            {
                this.SetState(PopupStateFull.SemiOpen);
                return;
            }
        }

        public PopupStateFull State { get; private set; }
        public async void SetState(PopupStateFull value)
        {
            if (State == value) { return; }
            State = value;
            if (State.PopupVisible) { IsVisible = State.PopupVisible; }
             await Task.Delay(0);
            invokeButton.RotateTo(State.ButtonRotation);
            await this.TranslateTo(0, State.PopupY());
            if (!State.PopupVisible) { IsVisible = State.PopupVisible; }
        }

        private void InvokeButton_Clicked(object sender, EventArgs e)
        {

            OnInvokeButtonClicked();
        }
    }

    public class PopupStateFull
    {
        public static PopupStateFull Closed = new PopupStateFull
        {
            ButtonRotation = 0,
            PopupY = () => ParentPageHeight,
            PopupVisible = false
        };
        public static PopupStateFull SemiOpen = new PopupStateFull
        {
            ButtonRotation = 0,
            // PopupY = () => ParentPageHeight - 160,
            //PopupY = () => ParentPageHeight - 60,
            PopupY = () => ParentPageHeight - 20,
            PopupVisible = true
        };
        public static PopupStateFull FullOpen = new PopupStateFull
        {
            ButtonRotation = 180,
            //PopupY = () => ParentPageHeight - 455,
            //PopupY = () => ParentPageHeight - 160,
            PopupY = () => ParentPageHeight - 260,
           
            PopupVisible = true
        };
        public static double ParentPageHeight { get; set; } = 0;
        public int ButtonRotation { get; set; }
        public Func<double> PopupY { get; set; }
        public bool PopupVisible { get; set; }
    }

}
