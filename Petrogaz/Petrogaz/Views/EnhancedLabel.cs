﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using Xamarin.Forms;

namespace Petrogaz.Views
{
    public class EnhancedLabel : Label
    {
        public static readonly BindableProperty NumLinesProperty =
            BindableProperty.Create(propertyName: nameof(NumLines),
                                    returnType: typeof(int),
                                    declaringType: typeof(EnhancedLabel),
                                    defaultValue: -1,
                                    propertyChanged: (b, o, n) =>
                                    {
                                        (b as EnhancedLabel).NumLinesChanged((int)n);
                                    });

        protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);
            if (propertyName == nameof(IsEnabledProperty))
            {
                Opacity = IsEnabled ? 1 : .9;
            }
        }
        public int NumLines
        {
            get { return (int)GetValue(NumLinesProperty); }
            set { SetValue(NumLinesProperty, value); }
        }
        private void NumLinesChanged(int newValue)
        {
            if (newValue == -1) { return; }
            LineBreakMode = Device.RuntimePlatform == Device.iOS ? LineBreakMode.TailTruncation : LineBreakMode.WordWrap;
        }

    }
}
