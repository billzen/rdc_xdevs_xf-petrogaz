﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Petrogaz.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoadingAnimationControl : Grid
	{
        #region IsAactive
        public bool IsActive
        {
            get { return (bool)GetValue(IsActiveProperty); }
            set { SetValue(IsActiveProperty, value); }
        }

        public static readonly BindableProperty IsActiveProperty =
                BindableProperty.Create(nameof(IsActive), 
                                        typeof(bool), 
                                        typeof(LoadingAnimationControl),
                                        false,
                                        propertyChanged: IsActivePropertyChanged);

        private static void IsActivePropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            ((LoadingAnimationControl)bindable).IsActivePropertyChanged((bool)newValue);
        }

        private void IsActivePropertyChanged(bool newValue)
        {
            IsVisible = newValue;
            animationView.IsPlaying = newValue;
        }
        #endregion


        public LoadingAnimationControl ()
		{
			InitializeComponent();
		}
	}
}