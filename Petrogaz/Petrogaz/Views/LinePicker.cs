﻿using Petrogaz.Abstractions;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Xamarin.Forms;

namespace Petrogaz.Views
{
    public class LinePicker : Picker, IAnimatedControlContent
    {
        public ICommand TriggeredCommand
        {
            get { return (ICommand)GetValue(TriggeredCommandProperty); }
            set { SetValue(TriggeredCommandProperty, value); }
        }

        public static readonly BindableProperty TriggeredCommandProperty =
           BindableProperty.Create(nameof(TriggeredCommand),
                                   typeof(ICommand),
                                   typeof(LinePicker));


        public TextAlignment HorizontalTextAlignment
        {
            get { return (TextAlignment)GetValue(HorizontalTextAlignmentProperty); }
            set { SetValue(HorizontalTextAlignmentProperty, value); }
        }
        public static readonly BindableProperty HorizontalTextAlignmentProperty =
            BindableProperty.Create(nameof(HorizontalTextAlignment), 
                                    typeof(TextAlignment), 
                                    typeof(LinePicker));

        public bool HasText => SelectedItem != null;
        public LinePicker()
        {
            if (Device.RuntimePlatform == Device.iOS)
            {
                HeightRequest = 30;
            }
        }
        protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);
            if (propertyName == nameof(Title))
            {
                Title = null;
            }
            else if (propertyName == nameof(IsFocused))
            {
                ControlTriggered(IsFocused);
            }
            else if (propertyName == nameof(SelectedItem))
            {
                ControlTriggered(SelectedItem != null);
            }
        }
        private void ControlTriggered(bool isFocused)
        {
            if (TriggeredCommand != null &&
                TriggeredCommand.CanExecute(null))
            {
                TriggeredCommand.Execute(isFocused);
            }
        }
    }
}
