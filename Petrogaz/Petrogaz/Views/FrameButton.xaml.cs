﻿using FFImageLoading.Forms;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Petrogaz.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FrameButton : Frame
    {
        public static readonly BindableProperty TextProperty =
            BindableProperty.Create(propertyName: nameof(Text),
                                    returnType: typeof(string),
                                    declaringType: typeof(FrameButton),
                                    defaultValue: string.Empty,
                                    propertyChanged: TextChanged);
        public static readonly BindableProperty TextColorProperty =
            BindableProperty.Create(propertyName: nameof(TextColor),
                                    returnType: typeof(Color),
                                    declaringType: typeof(FrameButton),
                                    defaultValue: Color.White,
                                    propertyChanged: TextColorChanged);
        public static readonly BindableProperty FontSizeProperty =
            BindableProperty.Create(propertyName: nameof(FontSize),
                                    returnType: typeof(int),
                                    declaringType: typeof(FrameButton),
                                    defaultValue: 16,
                                    propertyChanged: FontSizeChanged);
        public static readonly BindableProperty CommandProperty =
            BindableProperty.Create(propertyName: nameof(Command),
                                    returnType: typeof(ICommand),
                                    declaringType: typeof(FrameButton),
                                    defaultValue: null,
                                    propertyChanged: CommandChanged);
        public static readonly BindableProperty CommandParameterProperty =
            BindableProperty.Create(propertyName: nameof(CommandParameter),
                                    returnType: typeof(object),
                                    declaringType: typeof(FrameButton),
                                    defaultValue: null,
                                     propertyChanged: CommandParameterChanged);
        public static readonly BindableProperty ImageSourceProperty =
            BindableProperty.Create(propertyName: nameof(ImageSource),
                                    returnType: typeof(string),
                                    declaringType: typeof(FrameButton),
                                    defaultValue: null,
                                    propertyChanged: ImageSourceChanged);

        private static void TextChanged(BindableObject bindable, object oldValue, object newValue)
        {
            (bindable as FrameButton).TextChanged(oldValue as string, newValue as string);
        }
        private static void TextColorChanged(BindableObject bindable, object oldValue, object newValue)
        {
            (bindable as FrameButton).TextColorChanged((Color)oldValue, (Color)newValue);
        }
        private static void ImageSourceChanged(BindableObject bindable, object oldValue, object newValue)
        {
            (bindable as FrameButton).ImageSourceChanged(oldValue as string, newValue as string);
        }
        private static void CommandChanged(BindableObject bindable, object oldValue, object newValue)
        {
            (bindable as FrameButton).CommandChanged(oldValue as ICommand, newValue as ICommand);
        }
        private static void CommandParameterChanged(BindableObject bindable, object oldValue, object newValue)
        {
            (bindable as FrameButton).CommandParameterChanged(oldValue, newValue);
        }
        private static void FontSizeChanged(BindableObject bindable, object oldValue, object newValue)
        {
            (bindable as FrameButton).FontSizeChanged((int)oldValue, (int)newValue);
        }
        public CachedImage Image { get; private set; }
        public Label Label { get; private set; }
        public FrameButton()
        {
            InitializeComponent();
        }
        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }
        public Color TextColor
        {
            get { return (Color)GetValue(TextColorProperty); }
            set { SetValue(TextColorProperty, value); }
        }
        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }
        public object CommandParameter
        {
            get { return GetValue(CommandParameterProperty); }
            set { SetValue(CommandParameterProperty, value); }
        }
        public string ImageSource
        {
            get { return (string)GetValue(ImageSourceProperty); }
            set { SetValue(ImageSourceProperty, value); }
        }
        public int FontSize
        {
            get { return (int)GetValue(FontSizeProperty); }
            set { SetValue(FontSizeProperty, value); }
        }
        private void CreateImage()
        {
            Image = new CachedImage
            {
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.End
            };
            mainLayout.Children.Add(Image);
        }
        private void CreateLabel()
        {
            Label = new Label
            {
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
                HorizontalTextAlignment = TextAlignment.Center,
                VerticalTextAlignment = TextAlignment.Center,
                TextColor = TextColor,
                Text = Text
            };
            mainLayout.Children.Add(Label);
        }
        private void TextChanged(string oldValue, string newValue)
        {
            if (Label == null) { CreateLabel(); }
            Label.Text = newValue;
            if (Image != null)
            {
                Grid.SetRowSpan(Image, 1);
                Grid.SetRowSpan(Label, 1);
                Grid.SetRow(Label, 1);
                Label.VerticalOptions = LayoutOptions.Start;
            }
            else
            {
                Grid.SetRowSpan(Label, 2);
                Grid.SetRow(Label, 0);
            }
        }
        private void TextColorChanged(Color oldValue, Color newValue)
        {
            if (Label == null) { CreateLabel(); }
            Label.TextColor = newValue;
        }
        private void CommandChanged(ICommand oldValue, ICommand newValue)
        {
            tapRecogniser.Command = newValue;
        }
        private void CommandParameterChanged(object oldValue, object newValue)
        {
            tapRecogniser.CommandParameter = newValue;
        }
        private void FontSizeChanged(int oldValue, int newValue)
        {
            if (Label == null) { CreateLabel(); }
            Label.FontSize = newValue;
        }
        private void ImageSourceChanged(string oldValue, string newValue)
        {
            if (Image == null) { CreateImage(); }
            Image.Source = Xamarin.Forms.ImageSource.FromFile(newValue);
            if (Label != null)
            {
                Grid.SetRowSpan(Image, 1);
                Grid.SetRowSpan(Label, 1);
                Grid.SetRow(Label, 1);
                Label.VerticalOptions = LayoutOptions.Start;
            }
            else
            {
                Grid.SetRowSpan(Image, 2);
            }
        }
        protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);
            if (propertyName == nameof(IsEnabled) ||
               propertyName == nameof(IsEnabledProperty))
            {
                Opacity = IsEnabled ? 1 : .8;
            }
        }

    }
}