﻿using Petrogaz.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Petrogaz.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HomeMenuButton : Grid
	{
        #region Image
        public string Image
        {
            get { return (string)GetValue(ImageProperty); }
            set { SetValue(ImageProperty, value); }
        }
        public static readonly BindableProperty ImageProperty =
            BindableProperty.Create(nameof(Image), typeof(string), typeof(HomeMenuButton),
                propertyChanged: ImagePropertyChanged);

        private static void ImagePropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            ((HomeMenuButton)bindable).ImagePropertyChanged((string)newValue);
        }

        private void ImagePropertyChanged(string newValue)
        {
            image.Source = ImageSource.FromFile(AssetProvider.GetAsset(newValue));
        }
        #endregion

        #region LabelText
        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }
        public static readonly BindableProperty TextProperty =
            BindableProperty.Create(nameof(Text), typeof(string), typeof(HomeMenuButton),
                propertyChanged: TextPropertyChanged);

        private static void TextPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            ((HomeMenuButton)bindable).TextPropertyChanged((string)newValue);
        }

        private void TextPropertyChanged(string newValue)
        {
            label.Text = newValue;
        }
        #endregion

        #region GestureResture
        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }

        public static readonly BindableProperty CommandProperty =
            BindableProperty.Create(nameof(Command), typeof(ICommand), typeof(HomeMenuButton));
        
        public HomeMenuButton ()
		{
			InitializeComponent ();
        }

        public string CommandParameter
        {
            get { return (string)GetValue(CommandParameterProperty); }
            set { SetValue(CommandParameterProperty, value); }
        }

        public static readonly BindableProperty CommandParameterProperty =
            BindableProperty.Create(nameof(CommandParameter), typeof(string), typeof(HomeMenuButton));

        private void RecognizerTapped(object sender, EventArgs e)
        {
            Command?.Execute(CommandParameter);
        }

        #endregion
    }
}