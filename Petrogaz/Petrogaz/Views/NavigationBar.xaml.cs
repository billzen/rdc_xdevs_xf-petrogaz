﻿using Petrogaz.Helpers;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Petrogaz.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NavigationBar : Grid
    {
        #region LeftButtonImage
        public string LeftButtonImage
        {
            get { return (string)GetValue(LeftButtonImageProperty); }
            set { SetValue(LeftButtonImageProperty, value); }
        }
        public static readonly BindableProperty LeftButtonImageProperty =
            BindableProperty.Create(nameof(LeftButtonImage), typeof(string), typeof(NavigationBar),
                 propertyChanged: LeftButtonImagePropertyChanged);

        private static void LeftButtonImagePropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            ((NavigationBar)bindable).LeftButtonImagePropertyChanged((string)newValue);
        }

        private void LeftButtonImagePropertyChanged(string newValue)
        {
            leftButton.Source = ImageSource.FromFile(AssetProvider.GetAsset(newValue));
            leftButton.IsVisible = true;
        }

        #endregion

        #region RightButtonImage
        public string RightButtonImage
        {
            get { return (string)GetValue(RightButtonImageProperty); }
            set { SetValue(RightButtonImageProperty, value); }
        }
        public static readonly BindableProperty RightButtonImageProperty =
            BindableProperty.Create(nameof(RightButtonImage), typeof(string), typeof(NavigationBar),
                propertyChanged: RightButtonImagePropertyChanged);

        private static void RightButtonImagePropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            ((NavigationBar)bindable).RightButtonImagePropertyChanged((string)newValue);
        }

        private void RightButtonImagePropertyChanged(string newValue)
        {
            rightButton.Source = ImageSource.FromFile(AssetProvider.GetAsset(newValue));
            rightButton.IsVisible = true;
        }
        #endregion

        #region SectionTitle
        public string SectionTitle
        {
            get { return (string)GetValue(SectionTitleProperty); }
            set { SetValue(SectionTitleProperty, value); }
        }
        public static readonly BindableProperty SectionTitleProperty =
            BindableProperty.Create(nameof(SectionTitle), typeof(string), typeof(NavigationBar),
                propertyChanged: SectionTitlePropertyChanged);

        private static void SectionTitlePropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            ((NavigationBar)bindable).SectionTitlePropertyChanged((string)newValue);
        }

        private void SectionTitlePropertyChanged(string newValue)
        {
            sectionTitleLabel.Text = newValue;
            Grid.SetRowSpan(titleLabel, 1);
            Grid.SetRow(titleLabel, 1);
            titleLabel.VerticalOptions = LayoutOptions.Start;
        }
        #endregion

        #region Title
        public string Title
        {
            get { return (string)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }
        public static readonly BindableProperty TitleProperty =
            BindableProperty.Create(nameof(Title), typeof(string), typeof(NavigationBar),
                propertyChanged: TitlePropertyChanged);

        private static void TitlePropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            ((NavigationBar)bindable).TitlePropertyChanged((string)newValue);
        }

        private void TitlePropertyChanged(string newValue)
        {
            titleLabel.Text = newValue;
        }
        #endregion

        #region LeftButtonCommand
        public ICommand LeftButtonCommand
        {
            get { return (ICommand)GetValue(LeftButtonCommandProperty); }
            set { SetValue(LeftButtonCommandProperty, value); }
        }
        public static readonly BindableProperty LeftButtonCommandProperty =
            BindableProperty.Create(nameof(LeftButtonCommand), typeof(ICommand), typeof(NavigationBar),
                propertyChanged: (b, o, n) => (b as NavigationBar).LeftButtonCommandChanged((ICommand)n));

        private void LeftButtonCommandChanged(ICommand n)
        {
            leftButton.Command = n;
        }
        #endregion

        #region RightButtonCommand
        public ICommand RightButtonCommand
        {
            get { return (ICommand)GetValue(RightButtonCommandProperty); }
            set { SetValue(RightButtonCommandProperty, value); }
        }
        public static readonly BindableProperty RightButtonCommandProperty =
            BindableProperty.Create(nameof(RightButtonCommand), typeof(ICommand), typeof(NavigationBar),
                propertyChanged: (b, o, n) => (b as NavigationBar).RightButtonCommandChanged((ICommand)n));

        private void RightButtonCommandChanged(ICommand n)
        {
            rightButton.Command = n;
        }

        #endregion

        public NavigationBar()
        {
            InitializeComponent();
        }
    }
}