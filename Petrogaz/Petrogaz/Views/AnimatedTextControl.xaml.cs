﻿using Petrogaz.Abstractions;
using Petrogaz.Helpers;
using Petrogaz.Strategies;
using System;
using Prism.Ioc;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Petrogaz.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AnimatedTextControl : Grid
    {
        public IAnimatedControlContent Control
        {
            get { return (IAnimatedControlContent)GetValue(ControlProperty); }
            set { SetValue(ControlProperty, value); }
        }
        public static readonly BindableProperty ControlProperty =
            BindableProperty.Create(nameof(Control),
                                    typeof(IAnimatedControlContent),
                                    typeof(AnimatedTextControl),
                                    propertyChanged: (b, o, n) => (b as AnimatedTextControl).ControlChanged((IAnimatedControlContent)n));
        public IAnimatedLabelStrategy LabelStrategy
        {
            get { return (IAnimatedLabelStrategy)GetValue(LabelBehaviourProperty); }
            set { SetValue(LabelBehaviourProperty, value); }
        }
        public static readonly BindableProperty LabelBehaviourProperty =
            BindableProperty.Create(nameof(LabelStrategy),
                                    typeof(IAnimatedLabelStrategy),
                                    typeof(AnimatedTextControl));
        public string Placeholder
        {
            get { return (string)GetValue(PlaceholderProperty); }
            set { SetValue(PlaceholderProperty, value); }
        }
        public static readonly BindableProperty PlaceholderProperty =
            BindableProperty.Create(nameof(Placeholder),
                                    typeof(string),
                                    typeof(AnimatedTextControl),
                                    propertyChanged: (b, o, n) => (b as AnimatedTextControl).PlaceholderChanged((string)n));

        public Color LineColor
        {
            get { return (Color)GetValue(LineColorProperty); }
            set { SetValue(LineColorProperty, value); }
        }
        public static readonly BindableProperty LineColorProperty =
            BindableProperty.Create(nameof(LineColor),
                                    typeof(Color),
                                    typeof(AnimatedTextControl),
                                    Color.Gray,
                                    propertyChanged: (b, o, n) => (b as AnimatedTextControl).LineColorChanged((Color)n));
        public Color LabelColor
        {
            get { return (Color)GetValue(LabelColorProperty); }
            set { SetValue(LabelColorProperty, value); }
        }
        public static readonly BindableProperty LabelColorProperty =
            BindableProperty.Create(nameof(LabelColor),
                                    typeof(Color),
                                    typeof(AnimatedTextControl),
                                    propertyChanged: (b, o, n) => (b as AnimatedTextControl).LabelColorChanged((Color)n));
        public Color LabelTitleColor
        {
            get { return (Color)GetValue(LabelTitleColorProperty); }
            set { SetValue(LabelTitleColorProperty, value); }
        }
        public static readonly BindableProperty LabelTitleColorProperty =
            BindableProperty.Create(nameof(LabelTitleColor),
                                    typeof(Color),
                                    typeof(AnimatedTextControl),
                                    propertyChanged: (b, o, n) => (b as AnimatedTextControl).LabelTitleColorChanged((Color)n));


        private bool controlIsFocused;
        public AnimatedTextControl()
        {
            InitializeComponent();
            LabelStrategy = LabelStrategy ?? AppHelper.Container.Resolve<IAnimatedLabelStrategy>();
            LabelStrategy.Initialize(label);
            LineColorChanged(LineColor);
            label.SizeChanged += LabelDrawn;
        }

        private void LabelDrawn(object sender, EventArgs e)
        {
            label.SizeChanged -= LabelDrawn;
            LabelStrategy.Animate(label, !controlIsFocused);
        }

        private void ControlStateChanged(bool isFocused)
        {
            controlIsFocused = isFocused;
            if (Control == null || 
                (label.Text?.Length > 0 && 
                 (Control.HasText && !controlIsFocused)))
            {
                return;
            }
            LabelStrategy.Animate(label, !controlIsFocused);
        }
        private void ControlChanged(IAnimatedControlContent newValue)
        {
            if (Control != null)
            {
                Children.Remove(Control as View);
            }
            Children.Add(newValue as View);
            Control.TriggeredCommand = new Command<bool>(ControlStateChanged);
            label.FontSize = Control.FontSize;
            label.HorizontalTextAlignment = Control.HorizontalTextAlignment;
            LabelStrategy.DefaultFontSize = Control.FontSize;
            ControlStateChanged(controlIsFocused);
        }
        private void PlaceholderChanged(string newValue)
        {
            label.Text = newValue;
            ControlStateChanged(controlIsFocused);
        }
        private void LineColorChanged(Color newValue)
        {
            line.BackgroundColor = newValue;
        }
        private void LabelColorChanged(Color newValue)
        {
            if (!controlIsFocused)
            {
                label.TextColor = newValue;
            }
            LabelStrategy.PlaceholderStateColor = newValue;
        }
        private void LabelTitleColorChanged(Color newValue)
        {
            if (controlIsFocused)
            {
                label.TextColor = newValue;
            }
            LabelStrategy.TitleStateColor = newValue;
        }
    }
}