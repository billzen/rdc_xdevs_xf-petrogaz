﻿using Petrogaz.Helpers;
using System;
using System.Globalization;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xrx.Localization.Abstractions;

namespace Petrogaz.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RadioToggleControl : ContentView, ILocalizable
    {
        public int Selection
        {
            get { return (int)GetValue(SelectionProperty); }
            set { SetValue(SelectionProperty, value); }
        }
        public static readonly BindableProperty SelectionProperty =
            BindableProperty.Create("Selection", 
                typeof(int), 
                typeof(RadioToggleControl),
                propertyChanged:(b, o, n) => (b as RadioToggleControl).SelectionChanged());

        private void SelectionChanged()
        {
            UpdateForSelection();
        }

        public RadioToggleControl()
        {
            InitializeComponent();
            AppHelper.Localizer.Register(this);
            ChangeCulture(null);
            UpdateForSelection();
        }
        private void ButtonClicked(object sender, EventArgs e)
        {
            Selection = sender == button1 ? 0 : 1;
        }

        private void UpdateForSelection()
        {
            if(Selection == 0)
            {
                button1.Source = AssetProvider.GetAsset("radio_on.png");
                button2.Source = AssetProvider.GetAsset("radio_off.png");
            }
            else
            {
                button1.Source = AssetProvider.GetAsset("radio_off.png");
                button2.Source = AssetProvider.GetAsset("radio_on.png");
            }
        }

        public void ChangeCulture(CultureInfo culture)
        {
            label1.Text = "aaaa";
            label2.Text = "bbbbb";
        }
    }
}