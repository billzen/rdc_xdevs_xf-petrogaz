﻿using Petrogaz.Abstractions;
using Xamarin.Forms;

namespace Petrogaz.Strategies
{
    public class BaseAnimatedLabelStrategy : IAnimatedLabelStrategy
    {
        private double _defaultFontSize;
        
        /*
         *   Using odd font size causes bug in UWP
         */
        public double DefaultFontSize
        {
            get => _defaultFontSize;
            set => _defaultFontSize = value + (value % 2);
        }
        public Color PlaceholderStateColor { get; set; } = Color.Transparent;
        public Color TitleStateColor { get; set; } = Color.Transparent;
        public double DistaceFactor { get; set; } = 1;

        public void Initialize(Label label)
        {
            label.VerticalTextAlignment = TextAlignment.Center;
            DefaultFontSize = label.FontSize;

        }
        public void Animate(Label label, bool isPlaceholder)
        {
            if (isPlaceholder)
            {
                PlaceholderAnimation(label);
            }
            else
            {
                TitleAnimation(label);
            }
        }

        protected virtual void TitleAnimation(Label label)
        {
            label.VerticalTextAlignment = TextAlignment.End;
            label.TranslateTo(0, -label.Height * DistaceFactor, 250, Easing.SinOut);
            if(Device.RuntimePlatform != Device.UWP)
            {
                label.FontSize = DefaultFontSize * .8;
            }
            if (TitleStateColor != Color.Transparent)
            {
                label.TextColor = TitleStateColor;
            }
        }

        protected virtual void PlaceholderAnimation(Label label)
        {
            label.VerticalTextAlignment = TextAlignment.Center;
            label.TranslateTo(0, 0, 250, Easing.SinIn);
            label.FontSize = DefaultFontSize;
            if (PlaceholderStateColor != Color.Transparent)
            {
                label.TextColor = PlaceholderStateColor;
            }
        }
    }
}
