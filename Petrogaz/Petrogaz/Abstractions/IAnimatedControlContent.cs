﻿using System.Windows.Input;
using Xamarin.Forms;

namespace Petrogaz.Abstractions
{
    public interface IAnimatedControlContent
    {
        bool HasText { get; }
        ICommand TriggeredCommand { get; set; }
        double FontSize { get; set; }
        TextAlignment HorizontalTextAlignment { get; set; }
    }
}
