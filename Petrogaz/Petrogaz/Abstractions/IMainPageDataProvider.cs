﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Petrogaz.Abstractions
{
    public interface IMainPageDataProvider
    {
        string SomeText { get; }
        string GetInfoText();
        Task<List<string>> GetPersonList();
    }
}
