﻿using Xamarin.Forms;

namespace Petrogaz.Abstractions
{
    public interface IAnimatedLabelStrategy
    {
        void Animate(Label label, bool isPlaceholder);
        void Initialize(Label label);
        double DefaultFontSize { get; set; }
        double DistaceFactor { get; set; }
        Color PlaceholderStateColor { get; set; }
        Color TitleStateColor { get; set; }
    }
}
