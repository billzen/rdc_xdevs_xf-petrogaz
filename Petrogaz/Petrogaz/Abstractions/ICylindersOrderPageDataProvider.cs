﻿using Petrogaz.Models;
using Petrogaz.Models.Abstractions;
using System.Threading.Tasks;

namespace Petrogaz.Abstractions
{
    public interface ICylindersOrderPageDataProvider
    {
        Task<IClient> VerifyClient(IClient client);
       
    }
}
