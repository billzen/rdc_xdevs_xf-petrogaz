﻿using Petrogaz.Models;
using System.Threading.Tasks;

namespace Petrogaz.Abstractions
{
    public interface IAddResellerPageDataProvider
    {
        Task<ActionResult<bool>> VerifyClient(Client client);
    }
}
