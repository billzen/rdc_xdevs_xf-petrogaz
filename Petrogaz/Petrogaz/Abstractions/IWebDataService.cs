﻿using Petrogaz.Models;
using System.Threading.Tasks;

namespace Petrogaz.Abstractions
{
    public interface IWebDataService
    {
        Task<ActionResult<bool>> SendContactRequest(ContactRequest request);
    }
}
