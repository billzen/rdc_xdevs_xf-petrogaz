﻿using System;
using System.Collections.Generic;
using System.Text;
using Petrogaz.Models;
using System.Threading.Tasks;

namespace Petrogaz.Abstractions
{
    public interface IApiServices
    {
        Task<bool> RegisterUserAsync(string email, string password, string confirmPassword, string BaseApiAddress, string path);
        Task<string> login();
        Task<string> GetAsync(string accessToken, string BaseApiAddress, string path);
        Task PostAsync(object model, string accessToken, string BaseApiAddress, string path);
        Task PutAsync(object model, int modelId, string accessToken, string BaseApiAddress, string path);
        Task DeleteAsync(int modelId, string accessToken, string BaseApiAddress, string path);
        Task<string> SearchAsync(string keyword, string accessToken, string BaseApiAddress, string path);
    }
}