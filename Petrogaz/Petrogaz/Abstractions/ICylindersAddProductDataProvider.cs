﻿using Petrogaz.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Petrogaz.Abstractions
{
    public interface ICylindersAddProductDataProvider
    {
        List<Product> GetProducts();
    }
}
