﻿using Petrogaz.Models;
using System.Threading.Tasks;

namespace Petrogaz.Abstractions
{
    public interface IDataStore
    {
        ActionResult<string> GetTwoLetterCulture();
        ActionResult<bool> SetTwoLetterCulture(string cultureCode);
    }
}
