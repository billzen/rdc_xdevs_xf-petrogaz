﻿using System.Collections.Generic;

namespace Petrogaz.Abstractions
{
    public interface ITanksOrderPageDataProvider
    {
        List<T> GetTankCapacities<T>();
        List<T> GetRegions<T>();
    }
}
