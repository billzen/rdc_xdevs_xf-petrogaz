﻿namespace Petrogaz.Localization
{
    public static class Strings
    {
        #region TanksOrderPageViewModel
        public static string MeasurementUnitKey { get; } = "MeasurementUnit";
        public static string QuantityCostKey { get; } = "QuantityCost";
        public static string IOwnATankKey { get; } = "IOwnATank";
        public static string TankCapacityKey { get; } = "TankCapacity";
        public static string AddYourCommentsHereKey { get; } = "AddYourCommentsHere";
        public static string YourInfoKey { get; } = "YourInfo";
        public static string NameKey { get; } = "Name";
        public static string RegionKey { get; } = "Region";
        public static string PhoneKey { get; } = "Phone";
        public static string AddressKey { get; } = "Address";
        public static string OptionalKey { get; } = "Optional";
        public static string EmailKey { get; } = "Email";
        public static string SendKey { get; } = "Send";
        public static string TanksKey { get; } = "Tanks";
        public static string OrderKey { get; } = "Order";
        #endregion

        #region TanksCallPageViewModel
        public static string CallUsKey { get; } = "CallUs";
        public static string CallKey { get; } = "Call";
        #endregion

        #region SettingsPageViewModel
        public static string LanguageKey { get; } = "Language";
        public static string AreYouResellerKey { get; } = "AreYouReseller";
        public static string AddYourBusinessKey { get; } = "AddYourBusiness";
        public static string AddNowKey { get; } = "AddNow";
        public static string SettingsKey { get; } = "Settings";
        #endregion

        #region ContactPageViewModel
        public static string PetrogazAddressKey { get; } = "PetrogazAddress";
        public static string PetrogazPostalCodeKey { get; } = "PetrogazPostalCode";
        public static string TelKey { get; } = "Tel";
        public static string EmergencyKey { get; } = "Emergency";
        public static string ContactCommentPlaceholderKey { get; } = "ContactCommentPlaceholder";
        public static string ContactKey { get; } = "Contact";
        #endregion

        #region AddResellerPageViewModel
        public static string ResellerIDKey { get; } = "ResellerID";
        public static string TINKey { get; } = "TIN";
        public static string VerifyKey { get; } = "Verify";
        public static string PressTheButtonToAddKey { get; } = "PressTheButtonToAdd";
        public static string AddLocationKey { get; } = "AddLocation";
        #endregion

        #region HomePageViewModel
        public static string AutogasKey { get; } = "Autogas";
        public static string CylindersKey { get; } = "Cylinders";
        public static string LearnKey { get; } = "Learn";
        public static string GasStationsKey { get; } = "Gas Stations";
        #endregion

        #region CylindersOrderPageViewModel
        public static string ClientKey { get; } = "Client";
        public static string ClientIDKey { get; } = "ClientID";
        public static string YourOrderKey { get; } = "YourOrder";
        public static string AddProductsKey { get; } = "AddProducts";
        public static string EmptyFieldKey { get; } = "EmptyField";
        public static string NotFoundKey { get; } = "NotFound";
        #endregion



        #region CylindersAddProductPageViewModel
        public static string SelectProductKey { get; } = "SelectProduct";
        public static string QuantityKey { get; } = "Quantity";
        public static string AddProductKey { get; } = "AddProduct";
        public static string SingleProductValidationKey { get; } = "SingleProductValidation";
        public static string ProductSelectionValidationKey { get; } = "ProductSelectionValidation";
        public static string UpdateProductKey { get; } = "UpdateProduct";
        public static string DeleteKey { get; } = "Delete";
        public static string UpdateKey { get; } = "Update";
        public static string AddKey { get; } = "Add";
        public static string DeleteValidationKey { get; } = "DeleteValidation";
        #endregion

        public static string BranchesKey { get; } = "Branches";
        public static string InfoKey { get; } = "Info";
        public static string SpotsKey { get; } = "Spots";
        public static string ErrorKey { get; } = "Error";
        public static string GenericErrorMessageKey { get; } = "GenericErrorMessage";
        public static string MissingInputErrorMessageKey { get; } = "MissingInputErrorMessage";
        public static string SuccessKey { get; } = "Success";
        public static string MailSentKey { get; } = "MailSent";

        #region LearnStartPage
        public static string StartKey { get; } = "Start";
        public static string LearnAboutKey { get; } = "LearnAbout";
        public static string LiquidGasKey { get; } = "LiquidGas";
        public static string ByAnsweringToKey { get; } = "ByAnsweringTo";
        public static string QuestionsKey { get; } = "Questions";
        #endregion

        #region LearnQuestionPage
        public static string CorrectKey { get; } = "Correct";
        public static string IncorrectKey { get; } = "Incorrect";
        public static string QuestionKey { get; } = "Question";
        #endregion

        #region LearnAnswerPage
        public static string FinishKey { get; } = "Finish";
        public static string NextQuestionKey { get; } = "NextQuestion";
        #endregion

        #region LearnFinishPage
        public static string YourSuccessRateIsKey { get; } = "YourSuccessRateIs";
        public static string VeryGoodKey { get; } = "VeryGood";
        public static string LetsTryAgainKey { get; } = "LetsTryAgain";
        public static string RestartKey { get; } = "Restart";
        #endregion
    }
}
