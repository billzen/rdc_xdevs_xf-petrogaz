﻿using System.Collections.Generic;
using System.Globalization;

namespace Petrogaz.Localization
{
    public static class Cultures
    {
        public static CultureInfo EN = new CultureInfo("en");
        public static CultureInfo EL = new CultureInfo("el");

        public static List<CultureInfo> All => new List<CultureInfo>
        {
            EN, EL
        };
    }
}
