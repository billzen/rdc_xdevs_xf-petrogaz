﻿using Petrogaz.Abstractions;
using Petrogaz.Models;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace Petrogaz.Persistence
{
    public class PreferencesDataStore : IDataStore
    {
        public ActionResult<string> GetTwoLetterCulture()
        {
            string cultureCode = Preferences.Get("culture", null);
            ActionResult<string> result = new ActionResult<string>();
            if(cultureCode != null)
            {
                result.Data = cultureCode;
                result.Success = true;
            }
            return result;
        }
        public ActionResult<bool> SetTwoLetterCulture(string cultureCode)
        {
            Preferences.Set("culture", cultureCode);
            return new ActionResult<bool> { Success = true };
        }
    }
}
