﻿using Petrogaz.Abstractions;
using Petrogaz.Helpers;
using Petrogaz.Localization;
using Petrogaz.Models;
using Prism.Navigation;
using Prism.Services;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Xamarin.Forms;

namespace Petrogaz.ViewModels
{
    public class CylindersAddProductPageViewModel : ViewModelBase
    {
        private readonly ICylindersAddProductDataProvider dataProvider;
        private readonly IPageDialogService dialogService;

        public ICommand RightButtonCommand { get; set; }
        public ICommand DeleteCommand { get; set; }
        public ICommand MainButtonCommand { get; set; }
        public List<Product> ProductsList { get; set; }
        public bool IsUpdatingProduct { get; set; } = false;
        public Product SelectedProduct { get; set; }
        public string Quantity { get; set; }
        public Order Order { get; set; }
        public int UpdatableProductIndex { get; set; }
        public OrderProductRequest Request { get; set; }


        #region Texts
        public string Section => AppHelper.Localizer.GetString(Strings.CylindersKey);
        public string SelectProductText => AppHelper.Localizer.GetString(Strings.SelectProductKey);
        public string QuantityText => AppHelper.Localizer.GetString(Strings.QuantityKey);
        public string DeleteText => AppHelper.Localizer.GetString(Strings.DeleteKey);
        public string AddOrUpdateText { get; set; } = AppHelper.Localizer.GetString(Strings.AddKey);
        #endregion

        public CylindersAddProductPageViewModel(INavigationService navigationService, ICylindersAddProductDataProvider dataProvider, IPageDialogService dialogService) : base(navigationService)
        {
            this.dataProvider = dataProvider;
            this.dialogService = dialogService;
            ProductsList = dataProvider.GetProducts();
            Title = AppHelper.Localizer.GetString(Strings.AddProductKey);
            RightButtonCommand = new Command(RightButtonAction);
            DeleteCommand = new Command(DeleteAction);
            MainButtonCommand = new Command(MainButtonAction);
            RaisePropertyChanged(nameof(ProductsList));
        }

        private void MainButtonAction()
        {
            if (IsUpdatingProduct)
            {
                UpdateProduct();
            }
            else
            {
                AddProduct();
            }
        }
        private void RightButtonAction()
        {
            IsIdle = false;
            NavigationService.GoBackAsync();
            IsIdle = true;
        }
        private void AddProduct()
        {
            if (UserSelectionIsValid())
            {
                if (ProductAlreadyExists())
                {
                    dialogService.DisplayAlertAsync(null, AppHelper.Localizer.GetString(Strings.SingleProductValidationKey), null, "OK");
                }
                else
                {
                    AddOrderProductAndContinue();
                }
            }
            else
            {
                dialogService.DisplayAlertAsync(null, AppHelper.Localizer.GetString(Strings.ProductSelectionValidationKey), null, "OK");
            }
        }

        private bool UserSelectionIsValid()
        {
            bool success = int.TryParse(Quantity, out int result);
            return (success && result > 0 && result < 100 && SelectedProduct != null);
        }

        private bool ProductAlreadyExists()
        {
            return Request.ExistingProducts.Any(product => product.Id == SelectedProduct.Id);
        }

        private void AddOrderProductAndContinue()
        {
            OrderProductResponse response = new OrderProductResponse
            {
                OrderProduct = new OrderProduct
                {
                    Product = SelectedProduct,
                    Quantity = Quantity
                }
            };
            ProceedWithResponse(response);
        }

        private void UpdateProduct()
        {
            if (UserSelectionIsValid())
            {
                Request.ProductToUpdate.Quantity = Quantity;
                OrderProductResponse response = new OrderProductResponse
                {
                    Type = OrderProductResponseType.Update,
                    OrderProduct = Request.ProductToUpdate
                };
                ProceedWithResponse(response);
            }
            else
            {
                dialogService.DisplayAlertAsync(null, AppHelper.Localizer.GetString(Strings.ProductSelectionValidationKey), null, "OK");
            }
        }

        private async void DeleteAction()
        {
            bool ConfirmDeletion = await dialogService.DisplayAlertAsync(null, AppHelper.Localizer.GetString(Strings.DeleteValidationKey), "OK", "CANCEL");
            if (ConfirmDeletion)
            {
                OrderProductResponse response = new OrderProductResponse
                {
                    Type = OrderProductResponseType.Delete,
                    OrderProduct = Request.ProductToUpdate
                };
                ProceedWithResponse(response);
            }

        }
        public override void OnNavigatingTo(INavigationParameters parameters)
        {
            if(parameters.ContainsKey("Request"))
            {
                Request = parameters["Request"] as OrderProductRequest;
                if (Request.ProductToUpdate != null)
                {
                    SelectedProduct = ProductsList
                        .FirstOrDefault(product => product.Id == Request.ProductToUpdate.Product.Id);
                    Quantity = Request.ProductToUpdate.Quantity;
                    IsUpdatingProduct = true;
                    AddOrUpdateText = AppHelper.Localizer.GetString(Strings.UpdateKey);
                    RaisePropertyChanged(nameof(Title));
                    RaisePropertyChanged(nameof(SelectedProduct));
                    RaisePropertyChanged(nameof(IsUpdatingProduct));
                    RaisePropertyChanged(nameof(Quantity));
                    RaisePropertyChanged(nameof(AddOrUpdateText));
                }
            }
        }

        private void ProceedWithResponse(OrderProductResponse response)
        {
            IsIdle = false;
            var navParameters = new NavigationParameters();
            navParameters.Add("Response", response);
            NavigationService.GoBackAsync(navParameters);
            IsIdle = true;
        }
    }
}
