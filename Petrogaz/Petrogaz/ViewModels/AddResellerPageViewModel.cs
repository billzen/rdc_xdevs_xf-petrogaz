﻿using Petrogaz.Abstractions;
using Petrogaz.Helpers;
using Petrogaz.Localization;
using Petrogaz.Models;
using Prism.Navigation;
using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Petrogaz.ViewModels
{
    public class AddResellerPageViewModel : ViewModelBase
    {
        private readonly IAddResellerPageDataProvider dataProvider;

        public ICommand LocationButtonCommand { get; set; }
        public ICommand VerifyClientCommand { get; set; }
        
        public bool IsInLocationMode { get; set; }

        public string ResellerIDText => AppHelper.Localizer.GetString(Strings.ResellerIDKey);
        public string TINText => AppHelper.Localizer.GetString(Strings.TINKey);
        public string VerifyText => AppHelper.Localizer.GetString(Strings.VerifyKey);
        public string PressTheButtonText => AppHelper.Localizer.GetString(Strings.PressTheButtonToAddKey);

        public string ResellerID { get; set; }
        public string ResellerTIN { get; set; }
        public AddResellerPageViewModel(INavigationService navigationService,
            IAddResellerPageDataProvider dataProvider) : base(navigationService)
        {
            Title = AppHelper.Localizer.GetString(Strings.AddYourBusinessKey);
            LocationButtonCommand = new Command(async () => await GetLocation());
            VerifyClientCommand = new Command(async () => await VerifyClient());
            this.dataProvider = dataProvider;
        }

        private async Task VerifyClient()
        {
            ActionResult<bool> result = await dataProvider.VerifyClient(new Client
            {
                Id = ResellerID,
                TIN = ResellerTIN
            });
            if(result.Success)
            {
                if(!result.Data)
                {

                }
                else
                {
                    IsInLocationMode = true;
                    RaisePropertyChanged(nameof(IsInLocationMode));
                }
            }
            else
            {

            }
        }

        private Task GetLocation()
        {
            throw new NotImplementedException();
        }
    }
}
