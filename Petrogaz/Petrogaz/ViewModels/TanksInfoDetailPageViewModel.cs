﻿using Petrogaz.Helpers;
using Petrogaz.Localization;
using Petrogaz.Models;
using Prism.Navigation;

namespace Petrogaz.ViewModels
{
    public class TanksInfoDetailPageViewModel : ViewModelBase
    {
        public InfoSection Section { get; set; }
        public string SectionTitle => AppHelper.Localizer.GetString(Strings.TanksKey);
        public TanksInfoDetailPageViewModel(INavigationService navigationService) : base(navigationService)
        {
            Title = AppHelper.Localizer.GetString(Strings.InfoKey);
        }
        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);
            if(parameters.ContainsKey("section"))
            {
                Section = (InfoSection)parameters["section"];
                RaisePropertyChanged(nameof(Section));
            }
        }
    }
}
