﻿using Petrogaz.Helpers;
using Petrogaz.Localization;
using Petrogaz.Views;
using Prism.Navigation;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Petrogaz.ViewModels
{
    public class SettingsPageViewModel : ViewModelBase
    {

        public string LanguageText => AppHelper.Localizer.GetString(Strings.LanguageKey);
        public string AreYouResellerText => AppHelper.Localizer.GetString(Strings.AreYouResellerKey);
        public string AddYourBusinessText => AppHelper.Localizer.GetString(Strings.AddYourBusinessKey);
        public string AddNowText => AppHelper.Localizer.GetString(Strings.AddNowKey);
        public List<CultureInfo> AvailableCultures => Cultures.All;
        public ICommand AddResellerCommand { get; set; }

        private CultureInfo selectedCulture;
        public CultureInfo SelectedCulture
        {
            get => selectedCulture;
            set
            {
                if(value != null)
                {
                    selectedCulture = value;
                    AppHelper.Localizer.CurrentCulture = selectedCulture;
                    RaisePropertyChanged();
                }
            }
        }

        public SettingsPageViewModel(INavigationService navigationService) : base(navigationService)
        {
            Title = AppHelper.Localizer.GetString(Strings.SettingsKey);
            SelectedCulture = AppHelper.Localizer.CurrentCulture;
            AddResellerCommand = new Command(async () => await NavigateTo(nameof(AddResellerPage)));
        }
        public override void ChangeCulture(CultureInfo culture)
        {
            base.ChangeCulture(culture);
            Title = AppHelper.Localizer.GetString(Strings.SettingsKey);
            RaisePropertyChanged(nameof(LanguageText));
            RaisePropertyChanged(nameof(AreYouResellerText));
            RaisePropertyChanged(nameof(AddYourBusinessText));
            RaisePropertyChanged(nameof(AddNowText));
        }
    }
}
