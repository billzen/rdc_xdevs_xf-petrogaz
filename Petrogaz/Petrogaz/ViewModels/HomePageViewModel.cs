﻿using Petrogaz.Helpers;
using Petrogaz.Localization;
using Petrogaz.Views;
using Prism.Navigation;
using System;
using System.Globalization;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xrx.Localization.Abstractions;

namespace Petrogaz.ViewModels
{
    public class HomePageViewModel : ViewModelBase
    {

        public ICommand ButtonCommand { get; set; }
        public ICommand SettingsButtonCommand { get; set; }
        public string AutogasText => AppHelper.Localizer.GetString(Strings.AutogasKey);
        public string TanksText => AppHelper.Localizer.GetString(Strings.TanksKey);
        public string CylindersText => AppHelper.Localizer.GetString(Strings.CylindersKey);
        public string LearnText => AppHelper.Localizer.GetString(Strings.LearnKey);
        public string ContactText => AppHelper.Localizer.GetString(Strings.ContactKey);
        public HomePageViewModel(INavigationService navigationService) : base(navigationService)
        {
            ButtonCommand = new Command<string>(async (page) => await ButtonAction(page));
            SettingsButtonCommand = new Command(async () => await NavigateToSettings());
        }

        private async Task NavigateToSettings()
        {
            IsIdle = false;
            await NavigationService.NavigateAsync($"NavigationPage/{nameof(SettingsPage)}", null, true);
            IsIdle = true;
        }

        private async Task ButtonAction(string page)
        {
            if(!string.IsNullOrEmpty(page))
            {
                IsIdle = false;
                await NavigationService.NavigateAsync(page, null, true);
                IsIdle = true;
            }
        }
        public override void ChangeCulture(CultureInfo culture)
        {
            base.ChangeCulture(culture);
            RaisePropertyChanged(nameof(AutogasText));
            RaisePropertyChanged(nameof(TanksText));
            RaisePropertyChanged(nameof(CylindersText));
            RaisePropertyChanged(nameof(LearnText));
            RaisePropertyChanged(nameof(ContactText));
        }
    }
}
