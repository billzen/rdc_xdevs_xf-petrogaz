﻿using Petrogaz.Helpers;
using Petrogaz.Localization;
using Prism.Mvvm;

namespace Petrogaz.ViewModels
{
    public class CylindersTabbedPageViewModel : BindableBase
    {
        public string OrderText => AppHelper.Localizer.GetString(Strings.OrderKey);
        public string SpotsText => AppHelper.Localizer.GetString(Strings.SpotsKey);
        public CylindersTabbedPageViewModel()
        {

        }
	}
}
