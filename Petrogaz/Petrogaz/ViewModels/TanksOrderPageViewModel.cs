﻿using Petrogaz.Abstractions;
using Petrogaz.Helpers;
using Petrogaz.Localization;
using Prism.Navigation;
using System.Collections.Generic;

namespace Petrogaz.ViewModels
{
    public class TanksOrderPageViewModel : ViewModelBase
	{
        private readonly ITanksOrderPageDataProvider dataProvider;
        public List<string> TankCapacities { get; set; }
        public List<string> Regions { get; set; }
        public string MesurementUnitText => AppHelper.Localizer.GetString(Strings.MeasurementUnitKey);
        public string QuantityCostText => AppHelper.Localizer.GetString(Strings.QuantityCostKey);
        public string IOwnATankText => AppHelper.Localizer.GetString(Strings.IOwnATankKey);
        public string TankCapacityText => AppHelper.Localizer.GetString(Strings.TankCapacityKey);
        public string AddYourCommentsText => AppHelper.Localizer.GetString(Strings.AddYourCommentsHereKey);
        public string YourInfoText => AppHelper.Localizer.GetString(Strings.YourInfoKey);
        public string NameText => AppHelper.Localizer.GetString(Strings.NameKey);
        public string RegionText => AppHelper.Localizer.GetString(Strings.RegionKey);
        public string PhoneText => AppHelper.Localizer.GetString(Strings.PhoneKey);
        public string AddressOptionalText => $"{AppHelper.Localizer.GetString(Strings.AddressKey)} ({AppHelper.Localizer.GetString(Strings.OptionalKey)})";
        public string EmailOptionalText => $"{AppHelper.Localizer.GetString(Strings.EmailKey)} ({AppHelper.Localizer.GetString(Strings.OptionalKey)})";
        public string SendText => AppHelper.Localizer.GetString(Strings.SendKey);
        public string Section => AppHelper.Localizer.GetString(Strings.TanksKey);
        public TanksOrderPageViewModel(INavigationService navigationService, ITanksOrderPageDataProvider dataProvider) : base(navigationService)
        {
            this.dataProvider = dataProvider;
            TankCapacities = dataProvider.GetTankCapacities<string>();
            Regions = dataProvider.GetRegions<string>();
            Title = AppHelper.Localizer.GetString(Strings.OrderKey);
            RaisePropertyChanged(nameof(TankCapacities));
            RaisePropertyChanged(nameof(Regions));
        }
    }
}
