﻿using Petrogaz.Helpers;
using Petrogaz.Localization;
using Prism.Mvvm;

namespace Petrogaz.ViewModels
{
    public class TanksTabbedPageViewModel : BindableBase
	{
        public string OrderText => AppHelper.Localizer.GetString(Strings.OrderKey);
        public string BranchesText => AppHelper.Localizer.GetString(Strings.BranchesKey);
        public string InfoText => AppHelper.Localizer.GetString(Strings.InfoKey);
        public string CallText => AppHelper.Localizer.GetString(Strings.CallKey);

        public TanksTabbedPageViewModel()
        {
            
        }
	}
}
