﻿using Petrogaz.Helpers;
using Petrogaz.Localization;
using Petrogaz.Models.Abstractions;
using Petrogaz.Quiz;
using Petrogaz.Views;
using Prism.Navigation;
using System.Collections.Generic;
using System.Windows.Input;
using Xamarin.Forms;

namespace Petrogaz.ViewModels
{
    public class LearnQuestionPageViewModel : ViewModelBase
    {
        #region Properties
        public IQuiz Quiz { get; private set; }
        public string Question { get;  set; }

        public ICommand CorrectChoiceCommand { get; set; }
        public ICommand IncorrectChoiceCommand { get; set; }
        public ICommand GoToHomePageCommand { get; set; }
        #endregion

        #region Text Properties
        public string CorrectText => AppHelper.Localizer.GetString(Strings.CorrectKey);
        public string IncorrectText => AppHelper.Localizer.GetString(Strings.IncorrectKey);
        public string QuestionText => AppHelper.Localizer.GetString(Strings.QuestionKey);
        #endregion

        public LearnQuestionPageViewModel(INavigationService navigationService,
            IQuizService quizService) : base(navigationService)
        {
            Title = AppHelper.Localizer.GetString(Strings.LearnKey);
            CorrectChoiceCommand = new Command(CorrectChoice);
            IncorrectChoiceCommand = new Command(IncorrectChoice);
            GoToHomePageCommand = new Command(GoToHomePage);
            Quiz = quizService.GetQuiz();
            Question = $"{QuestionText} {Quiz.CurrentQuestionIndex + 1} / {Quiz.Questions.Count}";
            RaisePropertyChanged(nameof(Question));
            RaisePropertyChanged(nameof(Quiz));
        }

        private async void GoToHomePage()
        {
            IsIdle = false;
            await NavigationService.NavigateAsync("/" + nameof(HomePage));
            IsIdle = true;
        }

        private async void CorrectChoice()
        {
            Quiz.CurrentQuestion.UserAnswer = new QuizAnswer();
            Quiz.CurrentQuestion.UserAnswer.Data = new List<bool> { true, false };
            IsIdle = false;
            await NavigationService.NavigateAsync("/" + nameof(LearnAnswerPage));
            IsIdle = true;
        }

        private async void IncorrectChoice()
        {
            Quiz.CurrentQuestion.UserAnswer = new QuizAnswer();
            Quiz.CurrentQuestion.UserAnswer.Data = new List<bool> { false, true };
            IsIdle = false;
            await NavigationService.NavigateAsync("/" + nameof(LearnAnswerPage));
            IsIdle = true;
        }




    }
}
