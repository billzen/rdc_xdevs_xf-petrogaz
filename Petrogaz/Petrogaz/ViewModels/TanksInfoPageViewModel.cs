﻿using Petrogaz.Helpers;
using Petrogaz.Localization;
using Petrogaz.Models;
using Petrogaz.Views;
using Prism.Navigation;
using System;
using System.Collections.Generic;

namespace Petrogaz.ViewModels
{
    public class TanksInfoPageViewModel : ViewModelBase
	{
        public List<InfoSection> InfoSections { get; set; }
        private InfoSection selectedSection;
        public InfoSection SelectedSection
        {
            get => selectedSection;
            set
            {
                selectedSection = value;
                if(selectedSection != null)
                {
                    NavigateToSection(selectedSection);
                }
            }
        }


        public string SectionTitle => AppHelper.Localizer.GetString(Strings.TanksKey);
        public TanksInfoPageViewModel(INavigationService navigationService) : base(navigationService)
        {
            Title = AppHelper.Localizer.GetString(Strings.InfoKey);
            InfoSections = new List<InfoSection>
            {
                new InfoSection
                {
                    Title = AppHelper.Localizer.GetString("TanksInfoTitle1"),
                    Body = AppHelper.Localizer.GetString("TanksInfoBody1")
                },
                new InfoSection
                {
                    Title = AppHelper.Localizer.GetString("TanksInfoTitle2"),
                    Body = AppHelper.Localizer.GetString("TanksInfoBody2")
                }
            };
            RaisePropertyChanged(nameof(InfoSections));
        }
        private async void NavigateToSection(InfoSection section)
        {
            IsIdle = false;
            NavigationParameters parameters = new NavigationParameters
            {
                { "section", section }
            };
            await NavigationService.NavigateAsync(nameof(TanksInfoDetailPage), parameters);
            IsIdle = true;
        }
    }
}
