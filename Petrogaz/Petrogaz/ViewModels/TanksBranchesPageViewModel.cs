﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Petrogaz.ViewModels
{
	public class TanksBranchesPageViewModel : ViewModelBase
	{
        public TanksBranchesPageViewModel(INavigationService navigationService) : base(navigationService)
        {
        }
    }
}
