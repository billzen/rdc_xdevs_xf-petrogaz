﻿using Petrogaz.Helpers;
using Petrogaz.Localization;
using Petrogaz.Models.Abstractions;
using Petrogaz.Views;
using Prism.Navigation;
using System.Windows.Input;
using Xamarin.Forms;

namespace Petrogaz.ViewModels
{
    public class LearnAnswerPageViewModel : ViewModelBase
    {
        #region Properties
        public string AnswerIcon { get; set; }
        public IQuiz Quiz { get; private set; }
        public ICommand NextQuestionOrFinishQuizCommand { get; set; }
        public ICommand GoToHomePageCommand { get; set; }
        #endregion

        #region Text Properties
        public string ButtonText { get; set; }
        public string FinishText => AppHelper.Localizer.GetString(Strings.FinishKey);
        public string NextQuestionText => AppHelper.Localizer.GetString(Strings.NextQuestionKey);
        #endregion

        public LearnAnswerPageViewModel(INavigationService navigationService,
            IQuizService quizService) : base(navigationService)
        {
            Title = AppHelper.Localizer.GetString(Strings.LearnKey);
            Quiz = quizService.GetQuiz();
            ShowAnswer();
            NextQuestionOrFinishQuizCommand = new Command(NextQuestionOrFinishQuiz);
            GoToHomePageCommand = new Command(GoToHomePage);
        }

        private async void GoToHomePage()
        {
            IsIdle = false;
            await NavigationService.NavigateAsync("/" + nameof(HomePage));
            IsIdle = true;
        }

        private void ShowAnswer()
        {
            if (Quiz.CurrentQuestion.GetSuccessRate() < 1)
            {
                AnswerIcon = AssetProvider.GetAsset("false_icon.png");
            }
            else
            {
                AnswerIcon = AssetProvider.GetAsset("true_icon.png");
            }
            if (Quiz.CurrentQuestionIndex + 1 == Quiz.Questions.Count)
            {
                ButtonText = FinishText;
            }
            else
            {
                ButtonText = NextQuestionText;
            }
            
            RaisePropertyChanged(nameof(ButtonText));
            RaisePropertyChanged(nameof(AnswerIcon));
            RaisePropertyChanged(nameof(Quiz));
        }

        private async void NextQuestionOrFinishQuiz()
        {
            if (Quiz.MoveNext())
            {
                IsIdle = false;
                await NavigationService.NavigateAsync(nameof(LearnQuestionPage));
                IsIdle = true;
            }
            else
            {
                IsIdle = false;
                await NavigationService.NavigateAsync("/" + nameof(LearnFinishPage));
                IsIdle = true;
                
            }

        }


    }
}
