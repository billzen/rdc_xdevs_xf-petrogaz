﻿using Petrogaz.Helpers;
using Petrogaz.Localization;
using Petrogaz.Models.Abstractions;
using Petrogaz.Views;
using Prism.Navigation;
using System;
using System.Windows.Input;
using Xamarin.Forms;

namespace Petrogaz.ViewModels
{
    public class LearnFinishPageViewModel : ViewModelBase
    {
        #region Properties
        public float Score { get; set; }
        public IQuiz Quiz { get; private set; }
        public Color SuccessRateColor { get; private set; }
        public string ResultMessage { get; set; }
        public ICommand RestartQuizCommand { get; set; }
        public ICommand GoToHomePageCommand { get; set; }
        #endregion

        #region Text Proprties
        public string YourSuccessRateIsText => AppHelper.Localizer.GetString(Strings.YourSuccessRateIsKey) + Environment.NewLine;       
        public string RestartText => AppHelper.Localizer.GetString(Strings.RestartKey);
        #endregion

        public LearnFinishPageViewModel(INavigationService navigationService,
            IQuizService quizService) : base(navigationService)
        {
            Title = AppHelper.Localizer.GetString(Strings.LearnKey);
            Quiz = quizService.GetQuiz();
            FinishQuiz();
            RestartQuizCommand = new Command(RestartQuiz);
            GoToHomePageCommand = new Command(GoToHomePage);
        }
        private async void GoToHomePage()
        {
            IsIdle = false;
            await NavigationService.NavigateAsync("/" + nameof(HomePage));
            IsIdle = true;
        }

        private void FinishQuiz()
        {           
            Score = Quiz.GetSuccessRate() * 100;
            if (Score > 49)
            {
                SuccessRateColor = Color.FromHex("#80C342");
            }
            else
            {
                SuccessRateColor = Color.FromHex("#FF5A5A");
            }
            ResultMessage = Quiz.GetResultMessage();
            RaisePropertyChanged(nameof(Score));
            RaisePropertyChanged(nameof(ResultMessage));
            RaisePropertyChanged(nameof(SuccessRateColor));
        }

        private async void RestartQuiz()
        {
            Quiz.Reset();
            IsIdle = false;
            await NavigationService.NavigateAsync("/" + nameof(LearnStartPage));
            IsIdle = true;
        }

    }
}
