﻿using Petrogaz.Abstractions;
using Petrogaz.Helpers;
using Petrogaz.Localization;
using Petrogaz.Models;
using Petrogaz.Views;
using Prism.Navigation;
using Prism.Services;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Petrogaz.ViewModels
{
    public class CylindersOrderPageViewModel : ViewModelBase
    {
        private readonly ICylindersOrderPageDataProvider dataProvider;
        private readonly IPageDialogService dialogService;

        public Order Order { get; set; } 
        public ICommand AddProductsCommand { get; set; }
        public ICommand VerifyCommand { get; set; }
        public bool ClientVerified { get; set; }
        private OrderProduct _selectedProduct;
        public OrderProduct SelectedProduct
        {
            get { return _selectedProduct; }
            set
            {
                _selectedProduct = value;
                ValidateProductAndProceed();
            }
        }

        #region Texts
        public string ClientText => AppHelper.Localizer.GetString(Strings.ClientKey);
        public string Section => AppHelper.Localizer.GetString(Strings.CylindersKey);
        public string ClientIDText => AppHelper.Localizer.GetString(Strings.ClientIDKey);
        public string TINText => AppHelper.Localizer.GetString(Strings.TINKey);
        public string VerifyText => AppHelper.Localizer.GetString(Strings.VerifyKey);
        public string YourOrderText => AppHelper.Localizer.GetString(Strings.YourOrderKey);
        public string AddYourCommentsHereText => AppHelper.Localizer.GetString(Strings.AddYourCommentsHereKey);
        public string AddProductsText => AppHelper.Localizer.GetString(Strings.AddProductsKey);
        public string SendText => AppHelper.Localizer.GetString(Strings.SendKey);
        #endregion
        
        public CylindersOrderPageViewModel(INavigationService navigationService, ICylindersOrderPageDataProvider dataProvider, IPageDialogService dialogService) : base(navigationService)
        {
            Title = AppHelper.Localizer.GetString(Strings.OrderKey);
            this.dataProvider = dataProvider;
            this.dialogService = dialogService;
            AddProductsCommand = new Command(AddProductsAction);
            VerifyCommand = new Command(VerifyClient);
            Order = new Order
            {
                Client = new Client(),
                Products = new ObservableCollection<OrderProduct>()
            };
        }

        private async void VerifyClient()
        {
            if (string.IsNullOrEmpty((string)Order.Client.Id) || string.IsNullOrEmpty(Order.Client.TIN))
            {
                await dialogService.DisplayAlertAsync(null, AppHelper.Localizer.GetString(Strings.EmptyFieldKey), null, "OK");
                return;
            }
            IsIdle = false;
            Order.Client = await dataProvider.VerifyClient(Order.Client);
            IsIdle = true;
            if (Order.Client == null)
            {
                await dialogService.DisplayAlertAsync(null, AppHelper.Localizer.GetString(Strings.NotFoundKey), null, "OK");
                return;
            }
            ClientVerified = true;
            RaisePropertyChanged(nameof(Order));
            RaisePropertyChanged(nameof(ClientVerified));
        }

        private async void AddProductsAction()
        {
            var navParameters = new NavigationParameters();
            OrderProductRequest request = new OrderProductRequest
            {
                ExistingProducts = Order.Products.Select(orderProduct => orderProduct.Product).ToList()
            };
            await ProceedWithRequest(request);
        }
        private async void ValidateProductAndProceed()
        {
            if (SelectedProduct != null)
            {
                OrderProductRequest request = new OrderProductRequest
                {
                    ExistingProducts = Order.Products.Select(orderProduct => orderProduct.Product).ToList(),
                    ProductToUpdate = SelectedProduct.Clone() as OrderProduct
                };
                await ProceedWithRequest(request);
            }
        }

        private async Task ProceedWithRequest(OrderProductRequest request)
        {
            IsIdle = false;
            var navParameters = new NavigationParameters();
            navParameters.Add("Request", request);
            await NavigationService.NavigateAsync(nameof(CylindersAddProductPage), navParameters);
            IsIdle = true;
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            if (parameters.ContainsKey("Response"))
            {
                OrderProductResponse response = parameters["Response"] as OrderProductResponse;
                if (response.Type == OrderProductResponseType.Add)
                {
                    Order.Products = new ObservableCollection<OrderProduct>(
                        Order.Products.Concat(new[] { response.OrderProduct }).ToList());
                }
                else if (response.Type == OrderProductResponseType.Update)
                {
                    OrderProduct orderProduct = Order.Products.First(oProduct => oProduct.Product.Id == response.OrderProduct.Product.Id);
                    orderProduct.Quantity = response.OrderProduct.Quantity;
                    Order.Products = new ObservableCollection<OrderProduct>(Order.Products);
                }
                else
                {
                    List<OrderProduct> list = Order.Products.ToList();
                    list.RemoveAll(oProduct => oProduct.Product.Id == response.OrderProduct.Product.Id);
                    Order.Products = list;
                }
                RaisePropertyChanged(nameof(Order));
            }
        }
    }
}
