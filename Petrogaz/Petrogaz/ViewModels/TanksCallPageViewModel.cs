﻿using Petrogaz.Helpers;
using Petrogaz.Localization;
using Prism.Navigation;

namespace Petrogaz.ViewModels
{
    public class TanksCallPageViewModel : ViewModelBase
    {
        public string CallUsText => AppHelper.Localizer.GetString(Strings.CallUsKey);
        public string SectionTitle => AppHelper.Localizer.GetString(Strings.TanksKey);
        public TanksCallPageViewModel(INavigationService navigationService) : base(navigationService)
        {
            Title = AppHelper.Localizer.GetString(Strings.CallKey);

        }
    }
}
