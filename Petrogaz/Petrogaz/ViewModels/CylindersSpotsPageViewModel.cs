﻿using System;
using System.ComponentModel;
using System.Windows.Input;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;
using Prism.Navigation;
using Prism.Commands;
using Newtonsoft.Json;
using Petrogaz.Helpers;
using Petrogaz.Localization;
using Petrogaz.Views;
using Petrogaz.Services;
using Petrogaz.Models;
using Petrogaz.Abstractions;

namespace Petrogaz.ViewModels
{
	public class CylindersSpotsPageViewModel : ViewModelBase
	{

        #region Properties -Commands

        public DelegateCommand NavigateToFilters { get; set; }

        private readonly IApiServices _apiServices = new ApiServices();
        private readonly INavigationService _navigationService;


        public ICommand GoToHomePageCommand { get; set; }

        public string token = "";

        public ObservableCollection<Pin> Pins { get; set; }


        private bool _IsLoading;
        public bool IsLoading
        {
            get { return _IsLoading; }
            set
            {
                if (_IsLoading == value)
                    return;

                _IsLoading = value;
                RaisePropertyChanged(nameof(IsLoading));
            }
        }

        #endregion

        #region constructor
        public CylindersSpotsPageViewModel(INavigationService navigationService) : base(navigationService)
        {
            IsIdle = false;
            //Title = AppHelper.Localizer.GetString(Strings.GasStationsKey) ?? "Cylinders Stations";
            GoToHomePageCommand = new Command(GoToHomePage);
            Pins = new ObservableCollection<Pin>();
            NavigateToFilters = new DelegateCommand(() => NavigateToFiltersPage());
        }
        #endregion

        #region public methods

        public Position RandomLatLon()
        {
            Random random = new Random();

            int lat = random.Next(516400146, 630304598); //18.51640014679267 - 18.630304598192915
            int lon = random.Next(224464416, 341194152); //-72.34119415283203 - -72.2244644165039

            Position nrp = new Position(Convert.ToDouble("18." + lat), Convert.ToDouble("-72." + lon));

            return nrp;
        }


        public async Task populatePins()
        {
           
            if (Pins.Count > 0) return; //already loaded pins

            IsLoading = true;

            if (string.IsNullOrWhiteSpace(token))
            {
#if DEBUG
                token = "gvPi7pvvxoWYvcmZ8l4qKeppDRcn5wHvwMTUGnNA_5p80YoSpZFQMBiDClZsUgh-imFgWIXQGgLC8Yxxm0nrbJ5W9aetP__OLorQ9KCXJymVPFUekLIu5Vp2fcCRLjcZmJZD51MgsW16zZFfIc044TqE0HGLjLG8XztRbTsnTnNLkynte3fjeUv_hWCvJBftOn-eJxK-jsDXS5APDoS6cnWEGAnS30NzZl5uowgy5-etnmzWbjm5HG_OXfmbhWyu3IKuI5vmhiK1kbbcMYnwQA";
#else
                      token = await this._apiServices.login().ConfigureAwait(false);
#endif
            }

            if (!string.IsNullOrWhiteSpace(token))
            {
                List<GasStation> gasstations = new List<GasStation>();
                string gasStationJsonString = await this._apiServices.GetAsync(token, "https://www.petrogazapp.gr", "api/gasstations?language_id=2&language_id=1").ConfigureAwait(false);

                var datalist = JsonConvert.DeserializeObject<RootObjectGasStation>(gasStationJsonString);

                AddPinsToCollection(datalist);

                MessagingCenter.Send("pinsLoadedCylindersStations", "pinsLoadedCylindersStations");
            }

            IsLoading = false;

        }

        public void AddPinsToCollection(RootObjectGasStation datalist)
        {
            Pin aPin = new Pin();

            foreach (var i in datalist.gasstations)
            {

                if (i.latitude != 0 && i.longitude != 0)
                {
                    aPin = new Pin();
                    aPin.Address = i.address;
                    aPin.IsVisible = true;
                    aPin.Label = i.company_name;
                    aPin.Tag = "Phone: " + i.telephone1 + "  " + i.telephone2 + " Fax: " + i.fax;
                    aPin.Icon = BitmapDescriptorFactory.FromBundle("type2");
                    aPin.Position = new Position(i.latitude, i.longitude);
                    aPin.Type = PinType.Place;
                    Pins.Add(aPin);
                }

                #region elseregion dummy data

                // else
                // {
                //         aPin = new Pin();
                //         aPin.Address = "KSDGKJDGF";
                //         aPin.IsVisible = true;
                //         aPin.Label = " sdjhf ;jsdf h;js dhf;";
                //         aPin.Tag = "sdfs00 0s0dfsd 0000df00";
                //         aPin.Icon = BitmapDescriptorFactory.FromBundle("type2");
                //         aPin.Position = new Position(32.427579, 38.342017);
                //         aPin.Type = PinType.Place;
                //         Pins.Add(aPin);
                //
                //         //-----------------------------------------------------------------
                //
                //         aPin = new Pin();
                //         aPin.Address = "Microsoft";
                //         aPin.IsVisible = true;
                //         aPin.Label = "Microsoft Hyderabad";
                //         aPin.Tag = "Phone: 16161616  Fax: 838383838";
                //         aPin.Icon = BitmapDescriptorFactory.FromBundle("type2");
                //         aPin.Position = new Position(37.427579, 28.342017);
                //         aPin.Type = PinType.Place;
                //         Pins.Add(aPin);
                //
                //         //-----------------------------------------------------------------
                //
                //
                //         aPin = new Pin();
                //         aPin.Address = "Gachibowli";
                //         aPin.IsVisible = true;
                //         aPin.Label = "Wipro Hyderabad";
                //         aPin.Tag = "Phone: 456363  Fax: 567567";
                //         aPin.Icon = BitmapDescriptorFactory.FromBundle("type2");
                //         aPin.Position = new Position(47.427579, 28.342017);
                //         aPin.Type = PinType.Place;
                //         Pins.Add(aPin);
                //
                //         //-----------------------------------------------------------------
                //
                //
                //         aPin = new Pin();
                //         aPin.Address = "Test";
                //         aPin.IsVisible = true;
                //         aPin.Label = "Test Name";
                //         aPin.Tag = "Phone: 123123  fax: 234233453454";
                //         aPin.Icon = BitmapDescriptorFactory.FromBundle("type2");
                //         aPin.Position = new Position(42.427579, 29.342017);
                //         aPin.Type = PinType.Place;
                //         Pins.Add(aPin);
                //
                //     //-----------------------------------------------------------------
                //     IsLoading = false;
                //
                //     MessagingCenter.Send("pinsLoadedGasStations", "pinsLoadedGasStations");
                //
                // }
                #endregion
            }
        }

        #endregion

        #region private methods
        private async void  NavigateToFiltersPage()
        {
            await NavigationService.NavigateAsync("CylindersFilterPage",null,true,false);
        }

        private async void GoToHomePage()
        {
            IsIdle = false;
            await NavigationService.NavigateAsync("/" + nameof(HomePage));
            IsIdle = true;
        }
        #endregion
    }
}
