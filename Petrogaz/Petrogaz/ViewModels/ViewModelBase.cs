﻿using Petrogaz.Helpers;
using Prism.Mvvm;
using Prism.Navigation;
using System.Globalization;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xrx.Localization.Abstractions;

namespace Petrogaz.ViewModels
{
    public class ViewModelBase : BindableBase, INavigationAware, IDestructible, ILocalizable
    {
        private bool isIdle;
        public bool IsIdle
        {
            get { return isIdle; }
            set { SetProperty(ref isIdle, value); }
        }

        protected INavigationService NavigationService { get; private set; }

        private string _title;
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        public ICommand NavigateToHomeCommand { get; set; }
        public ICommand NavigateToBackCommand { get; set; }
        public ViewModelBase(INavigationService navigationService)
        {
            NavigationService = navigationService;
            AppHelper.Localizer.Register(this);
            NavigateToHomeCommand = new Command(NavigateToHome, () => IsIdle);
            NavigateToBackCommand = new Command(NavigateToBack, () => IsIdle);
            IsIdle = true;
        }

        public virtual void OnNavigatedFrom(INavigationParameters parameters)
        {

        }

        public virtual void OnNavigatedTo(INavigationParameters parameters)
        {

        }

        public virtual void OnNavigatingTo(INavigationParameters parameters)
        {

        }
        public virtual void ChangeCulture(CultureInfo culture)
        {

        }

        protected virtual async void NavigateToHome()
        {
            IsIdle = false;
            await NavigationService.GoBackAsync(useModalNavigation: true);
            IsIdle = true;
        }
        protected virtual async void NavigateToBack()
        {
            IsIdle = false;
            await NavigationService.GoBackAsync();
            IsIdle = true;
        }
        protected virtual async Task NavigateTo(string uri)
        {
            IsIdle = false;
            await NavigationService.NavigateAsync(uri);
            IsIdle = true;
        }

        public virtual void Destroy()
        {
            AppHelper.Localizer.Unregister(this);
        }

    }
}
