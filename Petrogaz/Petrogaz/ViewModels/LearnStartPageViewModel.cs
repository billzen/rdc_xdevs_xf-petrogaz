﻿using Petrogaz.Helpers;
using Petrogaz.Localization;
using Petrogaz.Models.Abstractions;
using Petrogaz.Views;
using Prism.Navigation;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Petrogaz.ViewModels
{
    public class LearnStartPageViewModel : ViewModelBase
    {
        #region Fields
        private readonly IQuizService quizService;
        #endregion

        #region Properties
        public IQuiz Quiz { get; private set; }
        public string ByAnsweringToXQuestions { get; set; }
        public ICommand StartQuizCommand { get; set; }
        public ICommand GoToHomePageCommand { get; set; }
        #endregion

        #region Text Properties
        public string StartText => AppHelper.Localizer.GetString(Strings.StartKey);
        public string ByAnsweringToText => AppHelper.Localizer.GetString(Strings.ByAnsweringToKey);
        public string QuestionsText => AppHelper.Localizer.GetString(Strings.QuestionsKey);
        #endregion

        public LearnStartPageViewModel(INavigationService navigationService,
            IQuizService quizService) : base(navigationService)
        {
            Title = AppHelper.Localizer.GetString(Strings.LearnKey);
            StartQuizCommand = new Command(StartQuiz);
            GoToHomePageCommand = new Command(GoToHomePage);
            this.quizService = quizService;
            new Command(async () => await Initialize()).Execute(null);
        }

        private async void GoToHomePage()
        {
            IsIdle = false;
            await NavigationService.GoBackAsync();
            IsIdle = true;
        }

        private async Task Initialize()
        {
            IsIdle = false;
            await quizService.Initialize();
            Quiz = quizService.GetQuiz();
            Quiz.MoveNext();
            ByAnsweringToXQuestions = $"{ByAnsweringToText} {Quiz.Questions.Count} {QuestionsText}";
            RaisePropertyChanged(nameof(ByAnsweringToXQuestions));
            IsIdle = true;
        }

        private async void StartQuiz()
        {
            IsIdle = false;
            await NavigationService.NavigateAsync("/" + nameof(LearnQuestionPage));
            IsIdle = true;
        }
    }
}
