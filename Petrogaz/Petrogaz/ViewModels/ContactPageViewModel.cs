﻿using Petrogaz.Abstractions;
using Petrogaz.Helpers;
using Petrogaz.Localization;
using Petrogaz.Models;
using Petrogaz.Validators;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xrx.Validators;

namespace Petrogaz.ViewModels
{
    public class ContactPageViewModel : ViewModelBase
    {
        #region Texts
        public string AddressText => AppHelper.Localizer.GetString(Strings.PetrogazAddressKey);
        public string PostalCodeText => AppHelper.Localizer.GetString(Strings.PetrogazPostalCodeKey);
        public string TelText => AppHelper.Localizer.GetString(Strings.TelKey);
        public string EmailText => AppHelper.Localizer.GetString(Strings.EmailKey);
        public string EmergencyText => AppHelper.Localizer.GetString(Strings.EmergencyKey);
        public string NameText => AppHelper.Localizer.GetString(Strings.NameKey);
        public string PhoneText => AppHelper.Localizer.GetString(Strings.PhoneKey);
        public string CommentText => AppHelper.Localizer.GetString(Strings.ContactCommentPlaceholderKey);
        public string SendText => AppHelper.Localizer.GetString(Strings.SendKey);
        #endregion

        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Body { get; set; }

        public DelegateCommand SendCommand { get; set; }

        private readonly IPageDialogService dialogService;
        private readonly IWebDataService webDataService;

        public ContactPageViewModel(INavigationService navigationService,
            IPageDialogService dialogService,
            IWebDataService webDataService) : base(navigationService)
        {
            Title = AppHelper.Localizer.GetString(Strings.ContactKey);
            SendCommand = new DelegateCommand(async () => await ValidateRequestAndSend())
                                .ObservesProperty(() => IsIdle);
            this.dialogService = dialogService;
            this.webDataService = webDataService;
        }

        private async Task ValidateRequestAndSend()
        {
            IsIdle = false;
            ContactRequest request = new ContactRequest
            {
                Name = Name,
                Phone = Phone,
                Email = Email,
                Body = Body
            };
            bool isValid = await ValidateRequest(request);
            if(isValid)
            {
                bool emailSent = await SendContactRequest(request);
                if(emailSent)
                {
                    NavigateToHome();
                }
            }
            IsIdle = true;
        }

        private async Task<bool> ValidateRequest(ContactRequest request)
        {
            ValidationResult result = new ContactRequestValidator().Validate(request);
            if (result.Exception != null)
            {
                await dialogService.DisplayAlertAsync(AppHelper.Localizer.GetString(Strings.ErrorKey),
                    AppHelper.Localizer.GetString(Strings.GenericErrorMessageKey), "OK");
                return false;
            }
            else if(!result.IsValid)
            {
                await dialogService.DisplayAlertAsync(AppHelper.Localizer.GetString(Strings.ErrorKey),
                    AppHelper.Localizer.GetString(Strings.MissingInputErrorMessageKey), "OK");
                return false;
            }
            return true;
        }

        private async Task<bool> SendContactRequest(ContactRequest request)
        {
            ActionResult<bool> result = await webDataService.SendContactRequest(request);
            if (result.Success)
            {
                await dialogService.DisplayAlertAsync(AppHelper.Localizer.GetString(Strings.SuccessKey),
                    AppHelper.Localizer.GetString(Strings.MailSentKey), "OK");
            }
            else
            {
                await dialogService.DisplayAlertAsync(AppHelper.Localizer.GetString(Strings.ErrorKey),
                    AppHelper.Localizer.GetString(Strings.GenericErrorMessageKey), "OK");
            }
            return result.Success;
        }
    }
}
