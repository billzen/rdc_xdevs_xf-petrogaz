﻿using Petrogaz.Models.Abstractions;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Petrogaz.Quiz
{
    public class MockQuizService : IQuizService
    {
        IQuiz quiz;
        public IQuiz GetQuiz()
        {
            return quiz;
        }

        public Task Initialize()
        {
            quiz = new LearnQuiz
            {
                Questions = new List<IQuizQuestion>
                {
                    new QuizMultipleChoiceQuestion
                    {
                        Description = "Σημείο ανάφλεξης (flashpoint), είναι η θερμοκρασία στην οποία το καύσιμο πρέπει να φτάσει για να εξατμιστεί αρκετή ποσότητα και να έχουμε έναυση. aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
                        Justification = "Αναφερόμαστε βέβαια για έναυση με την παρουσία κάποιας φλόγας",
                        Solution = new QuizAnswer
                        {
                            Data = new List<bool>{ false, true }
                        }
                    },new QuizMultipleChoiceQuestion
                    {
                        Description = "Σημείο ανάφλεξης (flashpoint), είναι η θερμοκρασία στην οποία το καύσιμο πρέπει να φτάσει για να εξατμιστεί αρκετή ποσότητα και να έχουμε έναυση.",
                        Justification = "Αναφερόμαστε βέβαια για έναυση με την παρουσία κάποιας φλόγας",
                        Solution = new QuizAnswer
                        {
                            Data = new List<bool>{ true, false }
                        }
                    },new QuizMultipleChoiceQuestion
                    {
                        Description = "Σημείο ανάφλεξης (flashpoint), είναι η θερμοκρασία στην οποία το καύσιμο πρέπει να φτάσει για να εξατμιστεί αρκετή ποσότητα και να έχουμε έναυση.",
                        Justification = "Αναφερόμαστε βέβαια για έναυση με την παρουσία κάποιας φλόγας",
                        Solution = new QuizAnswer
                        {
                            Data = new List<bool>{ true, false }
                        }
                    },new QuizMultipleChoiceQuestion
                    {
                        Description = "Σημείο ανάφλεξης (flashpoint), είναι η θερμοκρασία στην οποία το καύσιμο πρέπει να φτάσει για να εξατμιστεί αρκετή ποσότητα και να έχουμε έναυση.",
                        Justification = "Αναφερόμαστε βέβαια για έναυση με την παρουσία κάποιας φλόγας",
                        Solution = new QuizAnswer
                        {
                            Data = new List<bool>{ false, true }
                        }
                    },new QuizMultipleChoiceQuestion
                    {
                        Description = "Σημείο ανάφλεξης (flashpoint), είναι η θερμοκρασία στην οποία το καύσιμο πρέπει να φτάσει για να εξατμιστεί αρκετή ποσότητα και να έχουμε έναυση.",
                        Justification = "Αναφερόμαστε βέβαια για έναυση με την παρουσία κάποιας φλόγας",
                        Solution = new QuizAnswer
                        {
                            Data = new List<bool>{ true, false }
                        }
                    },new QuizMultipleChoiceQuestion
                    {
                        Description = "Σημείο ανάφλεξης (flashpoint), είναι η θερμοκρασία στην οποία το καύσιμο πρέπει να φτάσει για να εξατμιστεί αρκετή ποσότητα και να έχουμε έναυση.",
                        Justification = "Αναφερόμαστε βέβαια για έναυση με την παρουσία κάποιας φλόγας",
                        Solution = new QuizAnswer
                        {
                            Data = new List<bool>{ true, false }
                        }
                    },new QuizMultipleChoiceQuestion
                    {
                        Description = "Σημείο ανάφλεξης (flashpoint), είναι η θερμοκρασία στην οποία το καύσιμο πρέπει να φτάσει για να εξατμιστεί αρκετή ποσότητα και να έχουμε έναυση.",
                        Justification = "Αναφερόμαστε βέβαια για έναυση με την παρουσία κάποιας φλόγας",
                        Solution = new QuizAnswer
                        {
                            Data = new List<bool>{ false, true }
                        }
                    },new QuizMultipleChoiceQuestion
                    {
                        Description = "Σημείο ανάφλεξης (flashpoint), είναι η θερμοκρασία στην οποία το καύσιμο πρέπει να φτάσει για να εξατμιστεί αρκετή ποσότητα και να έχουμε έναυση.",
                        Justification = "Αναφερόμαστε βέβαια για έναυση με την παρουσία κάποιας φλόγας",
                        Solution = new QuizAnswer
                        {
                            Data = new List<bool>{ true, false }
                        }
                    },new QuizMultipleChoiceQuestion
                    {
                        Description = "Σημείο ανάφλεξης (flashpoint), είναι η θερμοκρασία στην οποία το καύσιμο πρέπει να φτάσει για να εξατμιστεί αρκετή ποσότητα και να έχουμε έναυση.",
                        Justification = "Αναφερόμαστε βέβαια για έναυση με την παρουσία κάποιας φλόγας",
                        Solution = new QuizAnswer
                        {
                            Data = new List<bool>{ false, true }
                        }
                    },new QuizMultipleChoiceQuestion
                    {
                        Description = "Σημείο ανάφλεξης (flashpoint), είναι η θερμοκρασία στην οποία το καύσιμο πρέπει να φτάσει για να εξατμιστεί αρκετή ποσότητα και να έχουμε έναυση.",
                        Justification = "Αναφερόμαστε βέβαια για έναυση με την παρουσία κάποιας φλόγας",
                        Solution = new QuizAnswer
                        {
                            Data = new List<bool>{ false, true }
                        }
                    }
                }
            };
            return Task.CompletedTask;
           
        }

        public void Reset()
        {
            quiz.Reset();
        }
    }
}
