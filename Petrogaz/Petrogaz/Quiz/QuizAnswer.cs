﻿using Petrogaz.Models.Abstractions;
using System.Collections.Generic;

namespace Petrogaz.Quiz
{
    public class QuizAnswer : IQuizAnswer
    {
        public object Data { get; set; }
    }
}
