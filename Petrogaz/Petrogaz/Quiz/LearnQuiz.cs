﻿using Petrogaz.Helpers;
using Petrogaz.Localization;
using Petrogaz.Models.Abstractions;
using System.Collections.Generic;
using System.Linq;

namespace Petrogaz.Quiz
{
    public class LearnQuiz : IQuiz
    {
        public List<IQuizQuestion> Questions { get; set; }
        public object Current => Questions[CurrentQuestionIndex];
        public IQuizQuestion CurrentQuestion => Current as IQuizQuestion;
        public int CurrentQuestionIndex { get; private set; }

        private string VeryGoodText => AppHelper.Localizer.GetString(Strings.VeryGoodKey);
        private string LetsTryAgainText => AppHelper.Localizer.GetString(Strings.LetsTryAgainKey);

        public LearnQuiz()
        {
            CurrentQuestionIndex = -1;
        }
        public string GetResultMessage()
        {
            if(GetSuccessRate() > .49)
            {
                return VeryGoodText ;
            }
            return LetsTryAgainText;
        }

        public float GetScore()
        {
            if (Questions != null && Questions.Count > 0)
            {
                return Questions.Sum(q => q.GetScore());
            }
            return 0;
        }

        public float GetSuccessRate()
        {
            if(Questions != null && Questions.Count > 0)
            {
                return Questions.Sum(q => q.GetSuccessRate()) / Questions.Count;
            }
            return 0;
        }

        public bool MoveNext()
        {
            if(Questions?.Count == 0 || CurrentQuestionIndex >= Questions?.Count - 1)
            {
                return false;
            }
            CurrentQuestionIndex++;
            return true;
        }

        public void Reset()
        {
            CurrentQuestionIndex = -1;
        }
    }
}
