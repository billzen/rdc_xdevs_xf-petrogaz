﻿using Petrogaz.Models.Abstractions;
using System.Collections.Generic;

namespace Petrogaz.Quiz
{
    public class QuizMultipleChoiceQuestion : IQuizQuestion
    {
        public string Description { get; set; }
        public string Justification { get; set; }
        public IQuizAnswer UserAnswer { get; set; }
        public IQuizAnswer Solution { get; set; }

        public float GetScore()
        {
            return GetSuccessRate() > .49 ? 1 : 0;
        }

        public float GetSuccessRate()
        {
            if(UserAnswer != null && Solution != null)
            {
                List<bool> answerData = UserAnswer.Data as List<bool>;
                List<bool> solutionData = Solution.Data as List<bool>;
                if(answerData.Count > 0 && answerData.Count == solutionData.Count)
                {
                    int correctCount = 0;
                    for (int i = 0; i < answerData.Count; i++)
                    {
                        if (answerData[i] == solutionData[i])
                        {
                            correctCount++;
                        }
                    }
                    return correctCount / answerData.Count;
                }
            }
            return 0;
        }
    }
}
