﻿using Petrogaz.Models;
using System.Text.RegularExpressions;
using Xrx.Validators;

namespace Petrogaz.Validators
{
    public class ContactRequestValidator : Validator<ContactRequest>
    {
        public ValidationResult Validate(ContactRequest request)
        {
            return Item(request)
                    .Should(r => r.Name.Length > 4)
                    .And(r => r.Phone.Length > 9)
                    .And(r => Regex.IsMatch(r.Email, @"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", RegexOptions.IgnoreCase))
                    .And(r => !string.IsNullOrEmpty(r.Body))
                    .Result;
        }
    }
}
