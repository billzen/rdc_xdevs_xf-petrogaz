﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Petrogaz.Helpers;
using Petrogaz.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Petrogaz.Abstractions;
using RestSharp;
using System.Web;
using System.Collections;

namespace Petrogaz.Services
{
    public class RegisterUserModel
    {
        public string Email { get; set; }

        public string Password { get; set; }

        public string ConfirmPassword { get; set; }
    }

    public class ApiServices : IApiServices
    {
        public static DateTime AccessTokenExpirationDate;

        public async Task<bool> RegisterUserAsync(
            string email, string password, string confirmPassword, string BaseApiAddress, string path)
        {
            var client = new HttpClient();

            var model = new RegisterUserModel
            {
                Email = email,
                Password = password,
                ConfirmPassword = confirmPassword
            };

            var json = JsonConvert.SerializeObject(model);

            HttpContent httpContent = new StringContent(json);

            httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync(
                BaseApiAddress + path, httpContent);

            if (response.IsSuccessStatusCode)
            {
                return true;
            }

            return false;
        }


        public async Task<string> login()
        {

            #region Old Rob
            //  string url = "https://www.petrogazapp.gr";
            // string client_id = "ef59dfb8 -5344-41b5-beba-d106cab09078";
            //  string client_secret = "29df0790-e2a8-4308-a5e1-c3a9792a36d7";

            //  //request token            
            //  var restclient = new RestClient(url);
            //  RestRequest request = new RestRequest("/oauth/authorize") { Method = Method.GET };
            //  request.AddHeader("Accept", "application/json");
            //  request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            //  request.AddParameter("client_id", client_id);
            //  request.AddParameter("response_type", "code");
            //  //request.AddParameter("redirect_uri", url);
            //
            //  var tResponse = restclient.Execute(request);
            //
            //  string statusDescr = tResponse.StatusDescription.ToLower();
            //
            //  if (statusDescr.Contains("not found")) return "";
            //
            //  //var code = tResponse.ResponseUri.Query.Replace("?code=", "");
            //  var code = Uri.UnescapeDataString(System.Web.HttpUtility.ParseQueryString(tResponse.ResponseUri.Query).Get("code"));
            //
            //  //request = new RestRequest("/oauth/token") { Method = Method.POST };
            //  request = new RestRequest("/api/token") { Method = Method.POST };
            //  request.AddHeader("Accept", "application/json");
            //  request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            //  request.AddParameter("client_id", client_id);
            //  request.AddParameter("client_secret", client_secret);
            //  request.AddParameter("code", code);
            //  request.AddParameter("grant_type", "authorization_code");
            //  //request.AddParameter("redirect_uri", url);
            //  tResponse = restclient.Execute(request);
            //  var responseJson = tResponse.Content;
            //  var token = JsonConvert.DeserializeObject<Dictionary<string, object>>(responseJson)["access_token"].ToString();
            //
            #endregion
            string url = "https://www.petrogazapp.gr";

            //multibanner keys
            string client_id = "ef59dfb8-5344-41b5-beba-d106cab09078";
            string client_secret = "29df0790-e2a8-4308-a5e1-c3a9792a36d7";


            //request token            
            var restclient = new RestClient(url);
            RestRequest request = new RestRequest("/oauth/authorize") { Method = Method.GET };
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddParameter("client_id", client_id);
            request.AddParameter("response_type", "code");
            //request.AddParameter("redirect_uri", url);
            var tResponse = restclient.Execute(request);

            var code = Uri.UnescapeDataString(HttpUtility.ParseQueryString(tResponse.ResponseUri.Query).Get("code"));

            request = new RestRequest("/api/token") { Method = Method.POST };
            request.AddHeader("Accept", "application/json");
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddParameter("client_id", client_id);
            request.AddParameter("client_secret", client_secret);
            request.AddParameter("code", code);
            request.AddParameter("grant_type", "authorization_code");
            tResponse = restclient.Execute(request);
            var responseJson = tResponse.Content;
            var token = JsonConvert.DeserializeObject<Dictionary<string, object>>(responseJson)["access_token"].ToString();

            await Task.Delay(0);

            string tokenget = token.ToString();
            return tokenget;
        }

        #region oldrobtest

        // public class LoginToken
        // {
        //     public string Access_token { get; set; }
        //     public string Expires_in { get; set; }
        //     public string Token_type { get; set; }
        //     public string Refresh_token { get; set; }
        // }

        // public async Task<string> LoginToServiceAsync(string AccessTokenUrl,string grandtype, string callback, string authurl, string clientId, string clientSecret)
        // {
        //     LoginToken token = new LoginToken { Access_token = "", Expires_in = "", Token_type = "", Refresh_token = "" };
        //     using (HttpClient client = new HttpClient())
        //     {
        //         var accept = "application/json";
        //         client.DefaultRequestHeaders.Add("Accept", accept);
        //         client.DefaultRequestHeaders.Add("client_id", clientId);
        //         client.DefaultRequestHeaders.Add("client_secret", clientSecret);
        //         client.DefaultRequestHeaders.Add("grant_type", grandtype);
        //         client.DefaultRequestHeaders.Add("callback", callback);
        //        // client.DefaultRequestHeaders.Add("Content-Type", "application/x-www-form-urlencoded");
        //         client.DefaultRequestHeaders.Add("response_type", "code");
        //
        //
        //         string postBody = @"client_id=" + clientId + "&client_secret=" + clientSecret + "&grant_type=" + grandtype + "&callback="+ callback;
        //
        //         var response = await client.PostAsync(AccessTokenUrl, new StringContent(postBody, Encoding.UTF8, "application/x-www-form-urlencoded")).ConfigureAwait(false);
        //
        //         if (response.IsSuccessStatusCode)
        //         {
        //             string responseStream = response.Content.ReadAsStringAsync().Result;
        //             token = JsonConvert.DeserializeObject<LoginToken>(responseStream);
        //             return token.Access_token;
        //         }
        //         else
        //         {
        //             return "ERROR";
        //         }
        //     }
        // }
        //
        #endregion


        public async Task<string> GetAsync(string accessToken, string BaseApiAddress, string path)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            var json = await client.GetStringAsync(BaseApiAddress + "/" + path);

            return json.ToString();
        }

        public async Task PostAsync(object model, string accessToken, string BaseApiAddress, string path)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            var json = JsonConvert.SerializeObject(model);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync(BaseApiAddress + "/" + path, content);
        }

        public async Task PutAsync(object model, int modelId, string accessToken, string BaseApiAddress, string path)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            var json = JsonConvert.SerializeObject(model);
            HttpContent content = new StringContent(json);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            var response = await client.PutAsync(
                BaseApiAddress + path + "/" + modelId, content);
        }

        public async Task DeleteAsync(int modelId, string accessToken, string BaseApiAddress, string path)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            var response = await client.DeleteAsync(
              BaseApiAddress + path + "/" + modelId);
        }


        public async Task<string> SearchAsync(string keyword, string accessToken, string BaseApiAddress, string path)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(
                "Bearer", accessToken);

            string json = await client.GetStringAsync(
                BaseApiAddress + path + "/" + keyword);

            return json.ToString();
        }
    }
}


