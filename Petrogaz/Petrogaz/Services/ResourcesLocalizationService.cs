﻿using Petrogaz.Helpers;
using Petrogaz.Localization;
using Plugin.Multilingual;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using Xrx.Localization;
using Xrx.Localization.Abstractions;
using Prism.Ioc;
using Petrogaz.Abstractions;
using Petrogaz.Models;
using System.Linq;

namespace Petrogaz.Services
{
    public class ResourcesLocalizationService : BaseLocalizationService
    {
        private readonly IDataStore dataStore;

        public override CultureInfo CurrentCulture
        {
            get => base.CurrentCulture;
            set
            {
                if (_currentCulture != null && _currentCulture.Equals(value)) { return; }
                _currentCulture = value;
                CrossMultilingual.Current.CurrentCultureInfo = _currentCulture;
                AppResources.Culture = _currentCulture;
                dataStore.SetTwoLetterCulture(_currentCulture.TwoLetterISOLanguageName);
                NotifyLocalizables();
            }
        }

        public ResourcesLocalizationService()
        {
            localizables = new List<ILocalizable>();
            dataStore = AppHelper.Container.Resolve<IDataStore>();
            ResolveActiveCulture();
        }

        private void ResolveActiveCulture()
        {
            ActionResult<string> result = dataStore.GetTwoLetterCulture();
            string letterCode;
            if(result.Success)
            {
                letterCode = result.Data;
            }
            else
            {
                letterCode = CrossMultilingual.Current.DeviceCultureInfo.TwoLetterISOLanguageName;
            }
            CultureInfo availableCulture = Cultures.All.FirstOrDefault(c => c.TwoLetterISOLanguageName == letterCode);
            CurrentCulture = availableCulture ?? Cultures.EL;
        }

        public override string GetString(object stringId) => GetString(stringId, CurrentCulture);

        public override string GetString(object stringId, CultureInfo culture) => stringId == null ? "" : AppResources.ResourceManager.GetString((string)stringId, culture);

        public override Task Initialize(IStringsProvider provider) => Task.CompletedTask;

    }
}
