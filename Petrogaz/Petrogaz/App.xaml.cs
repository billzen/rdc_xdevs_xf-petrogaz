﻿using Prism;
using Prism.Ioc;
using Petrogaz.ViewModels;
using Petrogaz.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xrx.Localization.Abstractions;
using Petrogaz.Services;
using Petrogaz.Abstractions;
using Petrogaz.PageDataProviders;
using Petrogaz.Helpers;
using Petrogaz.Strategies;
using Petrogaz.Persistence;
using Petrogaz.Models.Abstractions;
using Petrogaz.Quiz;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace Petrogaz
{
    public partial class App
    {
        public App() : this(null) { }

        public App(IPlatformInitializer initializer) : base(initializer) { }

        protected override async void OnInitialized()
        {
            ViewHelper.Current.Initialize();

            InitializeComponent();
            await NavigationService.NavigateAsync("HomePage");
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            #region Singletons
            containerRegistry.RegisterInstance<IDataStore>(new PreferencesDataStore());
            containerRegistry.RegisterInstance<ILocalizationService>(new ResourcesLocalizationService());
            containerRegistry.RegisterInstance<IQuizService>(new MockQuizService());
            containerRegistry.RegisterInstance<IWebDataService>(new WebDataService());
            #endregion

            containerRegistry.Register<IAnimatedLabelStrategy, BaseAnimatedLabelStrategy>();

            containerRegistry.Register<ITanksOrderPageDataProvider, MockTanksOrderPageDataProvider>();
            containerRegistry.Register<ICylindersOrderPageDataProvider, MockCylindersOrderPageDataProvider>();
            containerRegistry.Register<ICylindersAddProductDataProvider, MockCylindersAddProductPageDataProvider>(); 
            containerRegistry.Register<IAddResellerPageDataProvider, AddResellerPageDataProvider>(); 
            

            containerRegistry.RegisterForNavigation<NavigationPage>();
            containerRegistry.RegisterForNavigation<ContactPage, ContactPageViewModel>();
            containerRegistry.RegisterForNavigation<TanksCallPage, TanksCallPageViewModel>();
            containerRegistry.RegisterForNavigation<SettingsPage, SettingsPageViewModel>();
            containerRegistry.RegisterForNavigation<AddResellerPage, AddResellerPageViewModel>();
            containerRegistry.RegisterForNavigation<TanksOrderPage, TanksOrderPageViewModel>();
            containerRegistry.RegisterForNavigation<HomePage, HomePageViewModel>();
            containerRegistry.RegisterForNavigation<CylindersOrderPage, CylindersOrderPageViewModel>();
            containerRegistry.RegisterForNavigation<CylindersAddProductPage, CylindersAddProductPageViewModel>();
            containerRegistry.RegisterForNavigation<TanksTabbedPage, TanksTabbedPageViewModel>();
            containerRegistry.RegisterForNavigation<CylindersTabbedPage, CylindersTabbedPageViewModel>();
            containerRegistry.RegisterForNavigation<AutoStationsPage, AutoStationsPageViewModel>();
            containerRegistry.RegisterForNavigation<TanksInfoPage, TanksInfoPageViewModel>();
            containerRegistry.RegisterForNavigation<TanksBranchesPage, TanksBranchesPageViewModel>();
            containerRegistry.RegisterForNavigation<CylindersSpotsPage, CylindersSpotsPageViewModel>();
            containerRegistry.RegisterForNavigation<TanksInfoDetailPage, TanksInfoDetailPageViewModel>();
            containerRegistry.RegisterForNavigation<LearnStartPage, LearnStartPageViewModel>();
            containerRegistry.RegisterForNavigation<LearnQuestionPage, LearnQuestionPageViewModel>();
            containerRegistry.RegisterForNavigation<LearnAnswerPage, LearnAnswerPageViewModel>();
            containerRegistry.RegisterForNavigation<LearnFinishPage, LearnFinishPageViewModel>();
            //containerRegistry.RegisterForNavigation<CylindersSpotsMapPopupPage, CylindersSpotsMapPopupPageViewModel>();
            containerRegistry.RegisterForNavigation<CylindersFilterPage, CylindersFilterPageViewModel>();
        }
    }
}
