﻿using Petrogaz.Abstractions;
using Petrogaz.Models;
using Petrogaz.Models.Abstractions;
using System.Threading.Tasks;

namespace Petrogaz.PageDataProviders
{
    public class MockCylindersOrderPageDataProvider : ICylindersOrderPageDataProvider
    {
        public Task<IClient> VerifyClient(IClient client)
        {
            client.Name = "Vana";
            client.Address = "Hrous";
            client.Phone = "+30 123 45 67 890";
            client.Email = "Vana@mail.com";
            return Task.FromResult(client);
        }
    }
}
