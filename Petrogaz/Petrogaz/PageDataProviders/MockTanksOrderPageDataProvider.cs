﻿using Petrogaz.Abstractions;
using System;
using System.Collections.Generic;

namespace Petrogaz.PageDataProviders
{
    public class MockTanksOrderPageDataProvider : ITanksOrderPageDataProvider
    {
        public List<T> GetRegions<T>() 
        {
            List<string> regions = new List<string>
            {
                "Athens",
                "Piraeus",
                "Thesaloniki",
                "Patra"
            };
            return regions as List<T>;
        }

        public List<T> GetTankCapacities<T>()
        {
            List<string> capacities = new List<string>
            {
                "Less than 1750 Lt",
                "1750 Lt and more"
            };
            return capacities as List<T>;
        }
    }
}
