﻿using Petrogaz.Abstractions;
using Petrogaz.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Petrogaz.PageDataProviders
{
    public class MockCylindersAddProductPageDataProvider : ICylindersAddProductDataProvider
    {

       
        public List<Product> GetProducts()
        {
            List<Product> ProductsList = new List<Product>()
            {
                new Product
                {
                    Id = "0",
                    Name = "Compound 3kg"
                },
                new Product
                {
                    Id = "1",
                    Name = "Compound 2kg"
                },
                new Product
                {
                    Id = "2",
                    Name = "Compound 6kg"
                }
            };
            return ProductsList;
        }


    }
}


