﻿using Petrogaz.Abstractions;
using Petrogaz.Models;
using System;
using System.Threading.Tasks;

namespace Petrogaz.PageDataProviders
{
    public class AddResellerPageDataProvider : IAddResellerPageDataProvider
    {
        public Task<ActionResult<bool>> VerifyClient(Client client)
        {
            return Task.FromResult(new ActionResult<bool>
            {
                Success = true,
                Data = true
            });
        }
    }
}
