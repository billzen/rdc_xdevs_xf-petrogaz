﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Petrogaz.Abstractions;
using Petrogaz.Helpers;

namespace Petrogaz.PageDataProviders
{
    public class MockMainPageDataProvider : IMainPageDataProvider
    {
        public string SomeText => AppHelper.Localizer.GetString("Test");

        public string GetInfoText()
        {
            return "asjkdgjhsgad";
        }

        public async Task<List<string>> GetPersonList()
        {
            return new List<string>
            {
                "ASDASD", "ASDASDAS"
            };
        }
    }
}
