﻿
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;

namespace Petrogaz.Droid
{
    [Activity(Label = "Petrogaz", Icon = "@mipmap/ic_launcher", Theme = "@style/MainTheme.Splash", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class SplashActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            RequestedOrientation = ScreenOrientation.Portrait;
            base.OnCreate(savedInstanceState);

            var intent = new Intent(this, typeof(MainActivity));
            if (Intent.Extras != null)
            {
                intent.PutExtras(Intent.Extras);
            }
            intent.SetFlags(ActivityFlags.SingleTop);
            StartActivity(intent);
            Finish();
        }
    }
}