﻿using Android.Content;
using Android.Content.Res;
using Petrogaz.Droid.Renderers;
using Petrogaz.Views;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(LineEntry), typeof(LineEntryRendererDroid))]
namespace Petrogaz.Droid.Renderers
{
    public class LineEntryRendererDroid : EntryRenderer
    {
        public LineEntryRendererDroid(Context context) : base(context)
        {
        }
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            if (Control != null)
            {
                Control.BackgroundTintList = ColorStateList.ValueOf(Android.Graphics.Color.Transparent);
            }
        }
    }
}