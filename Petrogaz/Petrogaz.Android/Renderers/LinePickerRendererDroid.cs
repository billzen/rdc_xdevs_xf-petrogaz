﻿using System.ComponentModel;
using Android.Content;
using Android.Content.Res;
using Android.Graphics;
using Android.OS;
using Android.Views;
using Petrogaz.Droid.Renderers;
using Petrogaz.Views;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly:ExportRenderer(typeof(LinePicker), typeof(LinePickerRendererDroid))]
namespace Petrogaz.Droid.Renderers
{
    public class LinePickerRendererDroid : PickerRenderer
    {
        public LinePickerRendererDroid(Context context) : base(context) { }
        protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
        {
            base.OnElementChanged(e);
            if(Control != null)
            {
                if (Build.VERSION.SdkInt >= BuildVersionCodes.Lollipop)
                {
                    Control.BackgroundTintList = ColorStateList.ValueOf(Android.Graphics.Color.Transparent);
                }
                else
                {
                    Control.Background.SetColorFilter(Android.Graphics.Color.Transparent, PorterDuff.Mode.SrcAtop);
                }
                OnElementPropertyChanged(Element, new PropertyChangedEventArgs("HorizontalTextAlignment"));
            }
        }
        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            if (Control == null) { return; }
            if (e.PropertyName == "HorizontalTextAlignment")
            {
                System.Diagnostics.Debug.WriteLine((Element as LinePicker).HorizontalTextAlignment);
                System.Diagnostics.Debug.WriteLine(ConvertXamarinToAndroidAlignment((Element as LinePicker).HorizontalTextAlignment));
                Control.Gravity = ConvertXamarinToAndroidAlignment((Element as LinePicker).HorizontalTextAlignment);
            }
        }
        private GravityFlags ConvertXamarinToAndroidAlignment(Xamarin.Forms.TextAlignment alignment)
        {
            if (alignment == Xamarin.Forms.TextAlignment.Center)
            {
                return GravityFlags.Center;
            }
            if (alignment == Xamarin.Forms.TextAlignment.Start)
            {
                return GravityFlags.Left;
            }
            return GravityFlags.Right;
        }
    }
}