﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Petrogaz.Droid;
using Xamarin.Forms.GoogleMaps;

using Xamarin.Forms.GoogleMaps.Android.Factories;

using AndroidBitmapDescriptor = Android.Gms.Maps.Model.BitmapDescriptor;

using AndroidBitmapDescriptorFactory = Android.Gms.Maps.Model.BitmapDescriptorFactory;



public sealed class AccessNativeBitmapConfig : IBitmapDescriptorFactory

{

    public AndroidBitmapDescriptor ToNative(BitmapDescriptor descriptor)

    {

        int resId = 0;

        switch (descriptor.Id)

        {

            case "type2":

                resId = Resource.Drawable.badge_icon;

                break;

            case "type1":

                resId = Resource.Drawable.branch_icon_blue;

                break;

        }



        return AndroidBitmapDescriptorFactory.FromResource(resId);

    }

}