﻿using System.ComponentModel;
using Petrogaz.iOS.Renderers;
using Petrogaz.Views;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly:ExportRenderer(typeof(LinePicker), typeof(LinePickerRendererIOS))]
namespace Petrogaz.iOS.Renderers
{
    public class LinePickerRendererIOS : PickerRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
        {
            base.OnElementChanged(e);
            if (Control != null)
            {
                Control.BorderStyle = UITextBorderStyle.None;
            }
            OnElementPropertyChanged(Element, new PropertyChangedEventArgs("HorizontalTextAlignment"));
        }
        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            if (e.PropertyName == "HorizontalTextAlignment")
            {
                Control.TextAlignment = ConvertXamarinToIOSAlignment((Element as LinePicker).HorizontalTextAlignment);
            }
        }
        private UITextAlignment ConvertXamarinToIOSAlignment(TextAlignment alignment)
        {
            if (alignment == TextAlignment.Center)
            {
                return UITextAlignment.Center;
            }
            if (alignment == TextAlignment.Start)
            {
                return UITextAlignment.Left;
            }
            return UITextAlignment.Right;
        }
    }
}