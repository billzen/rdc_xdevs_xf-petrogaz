﻿
using Petrogaz.iOS.Renderers;
using Petrogaz.Views;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(LineEntry), typeof(LineEntryRendererIOS))]
namespace Petrogaz.iOS.Renderers
{
    public class LineEntryRendererIOS : EntryRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            if(Control != null)
            {
                Control.BorderStyle = UITextBorderStyle.None;
                Control.BackgroundColor = UIColor.Clear;
            }
        }
    }
}