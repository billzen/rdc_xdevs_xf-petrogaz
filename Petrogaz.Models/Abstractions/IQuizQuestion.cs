﻿namespace Petrogaz.Models.Abstractions
{
    public interface IQuizQuestion
    {
        string Description { get; set; }
        string Justification { get; set; }
        IQuizAnswer UserAnswer { get; set; }
        IQuizAnswer Solution { get; set; }
        float GetSuccessRate();
        float GetScore();
    }
}
