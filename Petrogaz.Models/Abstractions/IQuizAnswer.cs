﻿namespace Petrogaz.Models.Abstractions
{
    public interface IQuizAnswer
    {
        object Data { get; set; }
    }
}
