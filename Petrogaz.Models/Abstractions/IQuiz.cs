﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Petrogaz.Models.Abstractions
{
    public interface IQuiz : IEnumerator
    {
        List<IQuizQuestion> Questions { get; set; }
        IQuizQuestion CurrentQuestion { get; }
        int CurrentQuestionIndex { get; }
        float GetSuccessRate();
        float GetScore();
        string GetResultMessage();
    }
}
