﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Petrogaz.Models.Abstractions
{
    public interface IClient : IEntity
    {
        string Name { get; set; }
        string TIN { get; set; }
        Region Region { get; set; }
        string Phone { get; set; }
        string Address { get; set; }
        string Email { get; set; }
        bool OwnsTank { get; set; }
        double TankCapacity { get; set; }
    }
}
