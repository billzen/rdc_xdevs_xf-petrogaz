﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Petrogaz.Models.Abstractions
{
    public interface IProduct : IEntity
    {
        string Name { get; set; }
    }
}
