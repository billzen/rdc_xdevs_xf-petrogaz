﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Petrogaz.Models.Abstractions
{
    public interface IOrder
    {
        IClient Client { get; set; }
        IEnumerable<OrderProduct> Products { get; set; }
        string Comments { get; set; }
    }
}
