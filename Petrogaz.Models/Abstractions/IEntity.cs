﻿namespace Petrogaz.Models.Abstractions
{
    public interface IEntity
    {
        object Id { get; set; }
    }
}
