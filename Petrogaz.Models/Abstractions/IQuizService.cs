﻿using System.Threading.Tasks;

namespace Petrogaz.Models.Abstractions
{
    public interface IQuizService
    {
        Task Initialize();
        IQuiz GetQuiz();
        void Reset();
    }
}
