﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Petrogaz.Models
{
    public class ActionResult<T>
    {
        public Exception Exception { get; set; }
        public string Message { get; set; }
        public T Data { get; set; }
        public bool Success { get; set; }
    }
}
