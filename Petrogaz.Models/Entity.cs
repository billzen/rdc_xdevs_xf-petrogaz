﻿using Petrogaz.Models.Abstractions;

namespace Petrogaz.Models
{
    public class Entity : IEntity
    {
        public object Id { get; set; }
    }
}
