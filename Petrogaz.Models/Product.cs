﻿using Petrogaz.Models.Abstractions;

namespace Petrogaz.Models
{
    public class Product : Entity, IProduct
    {
        public string Name { get; set; }
    }
}
