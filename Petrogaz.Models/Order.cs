﻿using Petrogaz.Models.Abstractions;
using System.Collections.Generic;

namespace Petrogaz.Models
{
    public class Order : Entity, IOrder
    {
        public IClient Client { get; set; }
        public IEnumerable<OrderProduct> Products { get; set; }
        public string Comments { get; set; }
    }
}
