﻿namespace Petrogaz.Models
{
    public enum MeasurementUnit
    {
        Litres, Euros, Pieces
    }
}
