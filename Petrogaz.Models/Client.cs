﻿using Petrogaz.Models.Abstractions;

namespace Petrogaz.Models
{
    public class Client : Entity, IClient
    {
        public string Name { get; set; }
        public string TIN { get; set; }
        public Region Region { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public bool OwnsTank { get; set; }
        public double TankCapacity { get; set; }
    }
}
