﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Petrogaz.Models
{
    public class OrderProductResponse
    {
        public OrderProduct OrderProduct { get; set; }
        public OrderProductResponseType Type { get; set; }
    }
    public enum OrderProductResponseType
    {
        Add, Update, Delete
    }
}
