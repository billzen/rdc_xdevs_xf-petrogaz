﻿using Petrogaz.Models.Abstractions;
using System;

namespace Petrogaz.Models
{
    public class OrderProduct : ICloneable
    {
        public IProduct Product { get; set; }
        public MeasurementUnit MeasurementUnit { get; set; }
        public string Quantity { get; set; }

        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}
