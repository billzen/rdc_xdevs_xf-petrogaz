﻿namespace Petrogaz.Models
{
    public class InfoSection
    {
        public string Title { get; set; }
        public string Body { get; set; }
    }
}
