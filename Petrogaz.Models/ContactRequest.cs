﻿namespace Petrogaz.Models
{
    public class ContactRequest
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Body { get; set; }
    }
}
