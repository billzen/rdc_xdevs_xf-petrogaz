﻿using Petrogaz.Models.Abstractions;
using System.Collections.Generic;

namespace Petrogaz.Models
{
    public class OrderProductRequest
    {
        public List<IProduct> ExistingProducts { get; set; }
        public OrderProduct ProductToUpdate { get; set; }

    }
}
